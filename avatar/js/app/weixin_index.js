(function(window,document,undefined) {
    var  datatable=null;
    var aoColumns = [
        { "mDataProp": "id" ,"bSortable": true},
        { "mDataProp": "openid" ,"bSortable": false},
        { "mDataProp": "nickname" ,"bSortable": false},

        { "mDataProp": "logo" ,"bSortable": false},
        { "mDataProp": "code" ,"bSortable": false},

        { "mDataProp": "addtime" ,"bSortable": false},
        { "mDataProp": "updatetime" ,"bSortable": false},
        { "mDataProp": "state" ,"bSortable": false},
        { "mDataProp": "option" ,"bSortable": false}
    ];

    // $("#datatable thead input").on('input', function() {
    //     if(this.list && isNaN(this.value))
    //     {
    //         return false;
    //     }
    //     datatable.fnFilter(this.value, datatable.oApi._fnVisibleToColumnIndex(datatable.fnSettings(), $("thead input").index(this)));
    // });

    // $("#datatable thead input").each(function(i) {
    //     this.initVal = this.value;
    // });
    // $("#datatable thead input").focus(function() {
    //     if (this.className == "search_init") {
    //         this.className = "";
    //         this.value = "";
    //     }
    // });
    // $("#datatable thead input").blur(function(i) {
    //     if (this.value == "") {
    //         this.className = "search_init";
    //         this.value = this.initVal;
    //     }
    // });

    window.initWeixinIndex = initWeixinIndex;

    function initWeixinIndex(){
        initTable();
    }

    /**
     * [initTable 渲染表格]
     * @return {[type]} [description]
     */
    function initTable()
    {

        datatable = $('#datatable').dataTable(
        {
            "sDom" : "<'dt-top-row'lf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            //"sDom" : "<'dt-top-row'>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "bSortCellsTop" : true,
            "oTableTools" :
            {
                "aButtons" : [
                    "copy",
                    {
                        "sExtends" : "collection",
                        "sButtonText" : 'Save <span class="caret" />',
                        "aButtons" : ["csv", "xls", "pdf"]
                    }
                ],
                "sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
            "fnInitComplete" : function(oSettings, json)
            {
                // $(this)
                // .closest('#dt_table_tools_wrapper')
                // .find('.DTTT.btn-group')
                // .addClass('table_tools_group')
                // .children('a.btn')
                // .each(function()
                //     {
                //         $(this).addClass('btn-sm btn-default');
                //     }
                // );
                this.fnAdjustColumnSizing(true);
            },
            "fnDrawCallback": function ( oSettings )
            {
                bindEvent();
                //pageSetUp();
                //runEditable();
                //ideasCount();
            },
            "bFilter":true,
            "sEmptyTable": "Empty Data",
            "bServerSide": true,
            'bPaginate': true, //
            "bProcessing": true, //
            "oLanguage": langage,
//            "bStateSave" : true,
            "sAjaxSource": sAjaxSource,
            // "aaData": data,
            "sAjaxDataProp": "content.data",
            "iDisplayLength": 10,
            "sPaginationType": "bootstrap_full",
            "sScrollX": "100%",
            //"bRetrieve": true,
            "bScrollCollapse": true,
            //"sScrollX": "100%",
            //"sScrollXInner": "100%",
            //"bScrollCollapse": false,
            // "aaSorting":[[9,'desc']],
            "aoColumns": aoColumns,
            // "aoColumnDefs":
            // [
            //     {"bSortable": false, "aTargets": [ 0 ]},
            //     {"bSortable": false, "aTargets": [ 1 ]},
            //     {"bSortable": true, "aTargets": [ 2 ]},
            //     {"bSortable": false, "aTargets": [ 3 ]},
            //     {"bSortable": false, "aTargets": [ 4 ]},
            //     {"bSortable": false, "aTargets": [ 5 ]},
            //     {"bSortable": false, "aTargets": [ 6 ]},
            //     {"bSortable": false, "aTargets": [ 7 ]},
            //     {"bSortable": false, "aTargets": [ 8 ]},
            //     {"bSortable": false, "aTargets": [ 9 ]},
            //     {"bSortable": false, "aTargets": [ 10 ]}
            // ],
            // "fnServerData":retrieveData
        });
    }

    function getData( aoData ) {
        var dataSet = [];
        $.ajax({
            url:sAjaxSource,
            type: "POST",
            dataType: "json",
            data: { aoData: JSON.stringify(aoData) },
            async:false,
            success:function(data){
                logincheck(data);
                if( data.status==200 ){
                    dataTmp = data.content.data;
                    // dataSet.push(dataTmp);
                    var options = "";
                    for(var item in dataTmp){
                        var tmp = [];
                        var state = '';
                        var options = '';
                        var nickname = dataTmp[item]['nickname'] ? dataTmp[item]['nickname'] : '';
                        tmp = [ dataTmp[item]['id'] , nickname , dataTmp[item]['logo'] , dataTmp[item]['code'] , dataTmp[item]['money'] , dataTmp[item]['total_money'] , dataTmp[item]['today_money'] , dataTmp[item]['yesterday_money'], dataTmp[item]['addtime'] , state , options ];
                        dataSet.push(tmp);
                    }
                }
            }
        });
        return dataSet;
    }

    function retrieveData( sSource, aoData, fnCallback ) {
        var data = getData( aoData );
        if( !data ){
            data = [];
        }
        console.log(data);
        console.log(fnCallback);
        fnCallback( data );
    }

    var uid = '';
    function bindEvent(){
        $("a.mycode").click(function (e){
            uid = $(this).data('id');
            var code = $(this).data('code');
            code = code?code:"";
            $.SmartMessageBox({
                title : "设置专属推广码",
                content : "请输入6位推广码",
                inputValue :code,
                buttons : "[取消][确定]",
                input : "text",
                placeholder : code
            }, function(ButtonPress, Value) {
                if (ButtonPress == "确定") {
                    $.ajax({
                        url:sAjaxSource_code,
                        type: "POST",
                        dataType: "json",
                        data: { id:uid,code:Value },
                        async:true,
                        success:function(data){
                            logincheck(data);
                            if( data.status==200 ){
                                alert('设置成功');
                            }else{
                                alert(data.msg);
                            }
                        }
                    });
                }
            });

            e.preventDefault();
        })
    }
})(window,document,undefined);