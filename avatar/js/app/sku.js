(function(window,document,undefined) {
    var  datatable=null;
    var aoColumns = [
        { "mDataProp": "product_sku_id" ,"bSortable": false},
        { "mDataProp": "sku_title" ,"bSortable": false},
        { "mDataProp": "sku_original_price" ,"bSortable": false},
        { "mDataProp": "sku_price" ,"bSortable": false},
        { "mDataProp": "sku_score" ,"bSortable": false},
        { "mDataProp": "create_time" ,"bSortable": false},
        { "mDataProp": "update_time" ,"bSortable": false},
        { "mDataProp": "status" ,"bSortable": false},
        { "mDataProp": "options" ,"bSortable": false}
    ];


    window.initSku = initSku;

    function initSku(){
        initTable();
    }

    /**
     * [initTable 渲染表格]
     * @return {[type]} [description]
     */
    function initTable()
    {

        datatable = $('#datatable').dataTable(
        {
        	"sDom" : "<'dt-top-row'lf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            //"sDom" : "<'dt-top-row'>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "bSortCellsTop" : true,
            "oTableTools" :
            {
                "aButtons" : [
                    "copy",
                    {
                        "sExtends" : "collection",
                        "sButtonText" : 'Save <span class="caret" />',
                        "aButtons" : ["csv", "xls", "pdf"]
                    }
                ],
                "sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
            "fnInitComplete" : function(oSettings, json)
            {
                this.fnAdjustColumnSizing(true);
            },
            "fnDrawCallback": function ( oSettings )
            {
                bindEvent();
                //pageSetUp();
                //runEditable();
                //ideasCount();
            },
            "bFilter":true,
            "sEmptyTable": "Empty Data",
            "bServerSide": true,
            'bPaginate': true, //
            "bProcessing": true, //
            "oLanguage": langage,
//            "bStateSave" : true,
            "sAjaxSource": sAjaxSource,
            // "aaData": data,
            "sAjaxDataProp": "content.data",
            "iDisplayLength": 10,
            "sPaginationType": "bootstrap_full",
            "sScrollX": "100%",
            //"bRetrieve": true,
//            "bScrollCollapse": true,
            "sScrollX": "100%",
            "sScrollXInner": "100%",
            //"bScrollCollapse": false,
            // "aaSorting":[[9,'desc']],
            "aoColumns": aoColumns,

        });
    }



    function bindEvent(){

    }

})(window,document,undefined);