(function(window,document,undefined) {
    var  datatable=null;
    var aoColumns = [
        { "mDataProp": "tixian_id" ,"bSortable": true},
        { "mDataProp": "nickname" ,"bSortable": false},

        { "mDataProp": "money" ,"bSortable": false},
        { "mDataProp": "money2" ,"bSortable": false},

        { "mDataProp": "create_time" ,"bSortable": false},
        { "mDataProp": "update_time" ,"bSortable": false},
        { "mDataProp": "pay_type" ,"bSortable": false},
        { "mDataProp": "pay_no" ,"bSortable": false},
        { "mDataProp": "pay_account" ,"bSortable": false},

        { "mDataProp": "options" ,"bSortable": false}
    ];



    window.initTixianIndex = initTixianIndex;

    function initTixianIndex(){
        initTable();
    }

    /**
     * [initTable 渲染表格]
     * @return {[type]} [description]
     */
    function initTable()
    {

        datatable = $('#datatable').dataTable(
        {
            "sDom" : "<'dt-top-row'lf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            //"sDom" : "<'dt-top-row'>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "bSortCellsTop" : true,
            "oTableTools" :
            {
                "aButtons" : [
                    "copy",
                    {
                        "sExtends" : "collection",
                        "sButtonText" : 'Save <span class="caret" />',
                        "aButtons" : ["csv", "xls", "pdf"]
                    }
                ],
                "sSwfPath" : "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
            "fnInitComplete" : function(oSettings, json)
            {

                this.fnAdjustColumnSizing(true);
            },
            "fnDrawCallback": function ( oSettings )
            {
                bindEvent();
                //pageSetUp();
                //runEditable();
                //ideasCount();
            },
            "bFilter":true,
            "sEmptyTable": "Empty Data",
            "bServerSide": true,
            'bPaginate': true, //
            "bProcessing": true, //
            "oLanguage": langage,
//            "bStateSave" : true,
            "sAjaxSource": sAjaxSource,
            // "aaData": data,
            "sAjaxDataProp": "content.data",
            "iDisplayLength": 10,
            "sPaginationType": "bootstrap_full",
            "sScrollX": "100%",
            //"bRetrieve": true,
            "bScrollCollapse": true,
            //"sScrollX": "100%",
            //"sScrollXInner": "100%",
            //"bScrollCollapse": false,
            // "aaSorting":[[9,'desc']],
            "aoColumns": aoColumns,
        });
    }

    function getData( aoData ) {
        var dataSet = [];
        $.ajax({
            url:sAjaxSource,
            type: "POST",
            dataType: "json",
            data: { aoData: JSON.stringify(aoData) },
            async:false,
            success:function(data){
                logincheck(data);
                if( data.status==200 ){
                    dataTmp = data.content.data;
                    // dataSet.push(dataTmp);
                    var options = "";
                    for(var item in dataTmp){
                        var tmp = [];
                        var state = '';
                        var options = '';
                        var nickname = dataTmp[item]['nickname'] ? dataTmp[item]['nickname'] : '';
                        tmp = [ dataTmp[item]['id'] , nickname , dataTmp[item]['logo'] , dataTmp[item]['code'] , dataTmp[item]['money'] , dataTmp[item]['total_money'] , dataTmp[item]['today_money'] , dataTmp[item]['yesterday_money'], dataTmp[item]['addtime'] , state , options ];
                        dataSet.push(tmp);
                    }
                }
            }
        });
        return dataSet;
    }

    function retrieveData( sSource, aoData, fnCallback ) {
        var data = getData( aoData );
        if( !data ){
            data = [];
        }
        console.log(data);
        console.log(fnCallback);
        fnCallback( data );
    }

    function bindEvent(){
        var item = {
            '1':"btn-success",
            '2':"btn-info",
            '3':"btn-danger",
        }
        $(".status-menu li a").unbind().click(function(){
            var id = $(this).data('id');
            var status = $(this).data('status');
            var $id = $("#status-btn-"+id);
            var curr_status = $id.data('status');
            if(curr_status==status){
                return false;
            }
            var $id2 = $("#status-btn-r-"+id);
            $.ajax({
                url: changeStatus,
                type:'POST',
                data: "id=" + id + "&status=" + status,
                success: function (data) {
                    if( data.status == 200  ){
                    	//1申请中，2账号错误，3已结重新填写，4成功打款，5拒绝
                        if(status==1){
                            var txt = "申请中";
                            var clas = "btn-success";
                        }else if(status==4){
                            var txt = "已发放";
                            var clas = "btn-info";
                        }else if(status==5){
                            var txt = "已拒绝";
                            var clas = "btn-danger";
                        }
                        $id.html(txt);
                        $id.removeClass(item[curr_status]).addClass(clas).data("status",curr_status);
                        $id2.removeClass(item[curr_status]).addClass(clas);
                    }else {
                        alert(data.msg);
                        return false;
                    }
                }
            });
            return false;
        });
    }
})(window,document,undefined);