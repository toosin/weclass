//全局公用函数



/**
 * [appendZero 补0函数]
 * @param  {[string]} s [description]
 * @return {[type]}   [description]
 */
function appendZero(s)
{
    return ("00"+ s).substr((s+"").length);
}

/**
 * [Format_num 数据格式化]
 * @param {[type]} s [description]
 */
function Format_num(s)
{
   s = parseFloat((s + "").replace(/[^\d\.-]/g, ""))+ "";
   var l = s.split(".")[0].split("").reverse(),
   t = "";
   for(i = 0; i < l.length; i ++ )
   {
      t += l[i] + ((i + 1) % 3 == 0 && (i + 1) != l.length ? "," : "");
   }
   return t.split("").reverse().join("");
}

//警告tips
function warningBox( content,title,timeout ){
    title = title? title: "警告";
    content = content ? content : "";

    var data = {
        title : title,
        content : content,
        color : "rgb(196, 106, 105)",
        icon : "fa fa-bell"
    };
    if( !isNaN(timeout) ){
        data.timeout = timeout;
    }
    $.smallBox(data);
}
//提示tips
function noticeBox(content,title,timeout){
    title = title? title: "提示";
    timeout = timeout ? timeout : 3000;
    content = content ? content : "";
    var data = {
        title : title,
        content : content,
        color : "#5384AF",
        icon : "fa fa-bell"
    };

    if( !isNaN(timeout) ){
        data.timeout = timeout;
    }
    $.smallBox( data );
}

//login ajax 验证登陆
/**
 *所有通过ajax调用的数据。先见过这步的验证，如果未登录直接跳转，如果登陆，择交给后续function继续处理，^_^
 */
function   logincheck(str){
	try {
		var jsonarr = JSON.parse(str);
		if(jsonarr.status=="403"){//表示未登录，直接调走
			warningBox(jsonarr.msg)
			setTimeout(function(){window.location.href=jsonarr.url},2000);
		}
	} catch (e) {//如果转换失败，有可能是直接返回的html，可以继续运行
		// TODO: handle exception
	}
}

/**
 * [confirm_dialog 确认对话框]
 * @param  {[type]} title       [标题]
 * @param  {[type]} content     [内容]
 * @param  {[type]} success_fun [成功回调函数]
 * @param  {[type]} fail_fun    [失败回调函数]
 * @return {[type]}             [description]
 */
function confirm_dialog( title , content , success_fun , fail_fun ) {
  $('#dialog_confirm').find('p').html(content);
  $('#dialog_confirm').dialog({
    autoOpen : false,
    width : 600,
    resizable : false,
    modal : true,
    draggable : false,
    resizable : false,
    title : title,
    buttons : [{
      html : "&nbsp; 确定",
      "class" : "btn btn-danger",
      click : function() {
        if( typeof success_fun == 'function' ) {
          success_fun();
        }
        $(this).dialog("close");
      }
    }, {
      html : "<i class='fa fa-times'></i>&nbsp; 取消",
      "class" : "btn btn-default",
      click : function() {
        if( typeof fail_fun == 'function' ) {
          fail_fun();
        }
        $(this).dialog("close");
      }
    }]
  });

  $('#dialog_confirm').dialog('open');
}