<?php
namespace Com\Weixin;

/**
 * 微信支付接口
 * 
 * @author codebean
 *        
 */
class Pay
{

    public function __construct()
    {
        require_once "Pay/lib/WxPay.Api.php";
        require_once "Pay/unit/WxPay.JsApiPay.php";
        require_once dirname(__FILE__) . '/Pay/unit/log.php';
    }

    /**
     * 获取统一下单信息
     */
    public function getUnifiedOrder($openId, $trade_no, $money, $notify_url , $body, $detail, $attach, $tag)
    {
        // 统一下单
        $input = new WxPayUnifiedOrder();
        $input->SetBody($body);
        $input->SetDetail($detail);
        $input->SetAttach($attach);
        $input->SetOut_trade_no($trade_no);
        $input->SetTotal_fee($money);
        $input->SetTime_start(date("YmdHis"));
        $input->SetTime_expire(date("YmdHis", time() + 60000));
        $input->SetGoods_tag($tag);
        
        $url = $notify_url ? $notify_url : C('NOTIFY_URL');
        $input->SetNotify_url( $url );
        
        $input->SetTrade_type("JSAPI");
        $input->SetOpenid($openId);
        $order = WxPayApi::unifiedOrder($input);
        
        return $order;
    }

    public function getJsApiPay()
    {
        return new JsApiPay();
    }
    
    public function notify($callback, &$msg){
        $order = WxPayApi::notify($callback, $msg);
    }
}