<?php
namespace Com\Ping;

use Pingpp\Charge;
use Pingpp\Error\Base;
use Pingpp\Pingpp;
use Duobao\Controller\LogController as MyLog;

if (!function_exists('curl_init')) {
    throw new \Exception('Pingpp needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
    throw new \Exception('Pingpp needs the JSON PHP extension.');
}
if (!function_exists('mb_detect_encoding')) {
    throw new \Exception('Pingpp needs the Multibyte String PHP extension.');
}

// Pingpp singleton
require(dirname(__FILE__) . '/lib/Pingpp.php');

// Utilities
require(dirname(__FILE__) . '/lib/Util/Util.php');
require(dirname(__FILE__) . '/lib/Util/Set.php');
require(dirname(__FILE__) . '/lib/Util/RequestOptions.php');

// Errors
require(dirname(__FILE__) . '/lib/Error/Base.php');
require(dirname(__FILE__) . '/lib/Error/Api.php');
require(dirname(__FILE__) . '/lib/Error/ApiConnection.php');
require(dirname(__FILE__) . '/lib/Error/Authentication.php');
require(dirname(__FILE__) . '/lib/Error/InvalidRequest.php');
require(dirname(__FILE__) . '/lib/Error/RateLimit.php');
require(dirname(__FILE__) . '/lib/Error/Channel.php');

// Plumbing
require(dirname(__FILE__) . '/lib/JsonSerializable.php');
require(dirname(__FILE__) . '/lib/PingppObject.php');
require(dirname(__FILE__) . '/lib/ApiRequestor.php');
require(dirname(__FILE__) . '/lib/ApiResource.php');
require(dirname(__FILE__) . '/lib/SingletonApiResource.php');
require(dirname(__FILE__) . '/lib/AttachedObject.php');
require(dirname(__FILE__) . '/lib/Collection.php');

// Pingpp API Resources
require(dirname(__FILE__) . '/lib/Charge.php');
require(dirname(__FILE__) . '/lib/Refund.php');
require(dirname(__FILE__) . '/lib/RedEnvelope.php');
require(dirname(__FILE__) . '/lib/Event.php');
require(dirname(__FILE__) . '/lib/Transfer.php');
require(dirname(__FILE__) . '/lib/Customer.php');
require(dirname(__FILE__) . '/lib/Source.php');
require(dirname(__FILE__) . '/lib/Card.php');
require(dirname(__FILE__) . '/lib/Token.php');
require(dirname(__FILE__) . '/lib/CardInfo.php');
require(dirname(__FILE__) . '/lib/SmsCode.php');

// wx_pub OAuth 2.0 method
require(dirname(__FILE__) . '/lib/WxpubOAuth.php');

class Ping{
    protected $appid = '';

    public function __construct($apikey,$appid)
    {
        $this->appid = $appid;
        $this->setKey($apikey);
    }

    private function setKey($key){
        Pingpp::setApiKey($key);
    }

    /**
     * @param        $subject
     * @param        $body
     * @param        $amount
     * @param        $order_no
     * @param string $currency
     * @param        $extra
     * @param        $channel
     * @param        $client_ip
     * @param        $app       array
     *
     * @return      $charge     订单号
     */
    public function Create($subject,$body,$amount,$order_no,$currency='cny',$extra,$channel,$client_ip){
        #MyLog::write('进入create');
        try{
           $charge =  Charge::create(
                array(
                    'subject' =>$subject,
                    'body' => $body,
                    'amount' => $amount,
                    'order_no'=>$order_no,
                    'currency'  => $currency,
                    'extra'     => $extra,
                    'channel'   => $channel,
                    'client_ip' => $client_ip,
                    'app'       => array('id'=>$this->appid)
                )
            );
//            ob_start();
//            var_dump($charge);
//            MyLog::write(ob_get_contents());
            #MyLog::write((String)$charge);
            return  $charge;
        }catch (Base $e){
            header('Status: ' . $e->getHttpStatus());
            // 捕获报错信息
//            MyLog::write($e->getHttpBody());
            echo $e->getHttpBody();
        }
    }
}