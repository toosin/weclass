<?php
namespace Com\OSS;

use Com\OSS\Core\OssException;
use Com\OSS\Core\OssUtil;
use Com\OSS\Model\RefererConfig;

/**
 *  配置类
 *
 * Class Config
 * @package Com\OSS
 */
final class Config
{
    const OSS_ACCESS_ID = 'cW5oi2442ZGRnF2z';
    const OSS_ACCESS_KEY = '4ebKSZO0uBWla2qsKgsWmUcdg7Szfx';
//    const OSS_ENDPOINT = 'oss-cn-beijing.aliyuncs.com';
    const OSS_BUCKET = 'xiaoyutech-';


    public static function getEndPoint($position){
        $positionList = array(
            "hangzhou" =>"oss-cn-hangzhou.aliyuncs.com",
            "shanghai" => "oss-cn-shanghai.aliyuncs.com",
            "qingdao" => "oss-cn-qingdao.aliyuncs.com",
            "beijing" => "	oss-cn-beijing.aliyuncs.com",
            "shenzhen" => "oss-cn-shenzhen.aliyuncs.com",
            "hongkong" => "oss-cn-hongkong.aliyuncs.com",#香港数据中心
            "SiliconValley" => "oss-us-west-1.aliyuncs.com",#美国硅谷数据中心
            "Virginia"=>"oss-us-east-1.aliyuncs.com",#美国弗吉尼亚数据中心
            "Singapore"=>"oss-ap-southeast-1.aliyuncs.com",#亚太（新加坡）数据中心
        );

        if(array_key_exists($position,$positionList)){
            return $positionList[$position];
        }else{
            echo "不存在的地区";
            exit(0);
        }
    }

    /* end of config */
}


/**
 * 客户端类
 *
 * Class Client
 * @package Com\OSS
 */
class Client{
    const accessKeyId = Config::OSS_ACCESS_ID;
    const accessKeySecret = Config::OSS_ACCESS_KEY;
    const bucket = Config::OSS_BUCKET;

    private $client = '';

    #构造方法，参数值beijing，创建bucket需要endposition
    public function __construct($position = "beijing",$accessKeyId='',$accessKeySecret=''){
        try {
            if(!$accessKeyId || !$accessKeySecret){
                #endpoint 指创建bucket的地域，默这里华北2地区,endpoint不能为空
                $ossClient = new OssClient(self::accessKeyId, self::accessKeySecret, Config::getEndPoint($position), false);
            }else{
                $ossClient = new OssClient($accessKeyId, $accessKeySecret, Config::getEndPoint($position), false);
            }
        } catch (OssException $e) {
            printf(__FUNCTION__ . "creating OssClient instance: FAILED\n");
            printf($e->getMessage() . "\n");
            exit();
        }
        $this->client = $ossClient;
    }

    /** * * * * * * * * * * * * * * * * * * * * * *
     *  bucket options
     *
     *  bucket 的操作，bucket是oss的object存储的namespace
     *
     *
     * * * * * * * * * * * * * * * * * * * * * * * */

    #创建bucket的name
    private static function getBucketName()
    {
        $suffix = '';
        for ($i = 1;$i <= 6; $i++){
            $suffix = $suffix.chr(rand(97,122));
        }

        return self::bucket.$suffix;
    }
    #创建bucket,所有创建的bucket都是public-read的
    public function createBucket($bucket_name = ""){
        $bucket_name = $bucket_name?$bucket_name:self::getBucketName();

        try{
           return $this->client->createBucket($bucket_name,OssClient::OSS_ACL_TYPE_PUBLIC_READ);
        }catch (\Exception $e){
            $message = $e->getMessage();
            if (OssUtil::startsWith($message, 'http status: 403')) {
                echo "Please Check your AccessKeyId and AccessKeySecret" . "\n";
                exit(0);
            } elseif (strpos($message, "BucketAlreadyExists") !== false) {
                echo "Bucket already exists. Please check whether the bucket belongs to you, or it was visited with correct endpoint. " . "\n";
                exit(0);
            }
        }

//        print(__FUNCTION__ . ": OK" . "\n");
    }

    /**
     * 创建虚拟目录
     * #如果执行 成功返回NULL
     *
     * @param $bucket
     * @param $object
     * @return null|string
     */
    public function createDir($bucket,$object){
        if(empty($bucket) || empty($object)){
            return "param error";
        }

        #  $object = div/mengbao
        return  $this->client->createObjectDir($bucket,$object);
    }

    // 判断bucket是否存在
    public function doesBucketExist($bucket){
        if(empty($bucket)){
            return "bucket not allowed empty";
        }
        return $this->client->doesBucketExist($bucket);
    }

    #列出list bucket
    public function listBuckets(){
        return $this->client->listBuckets()->getBucketList();
    }

    #修改bucket的acl
    public function putBucketAcl($bucket,$acl){
        if(empty($bucket)){
            return "bucket not allowed empty";
        }
       return $this->client->putBucketAcl($bucket,$acl);
    }

    #获取bucket的ACL
    public function getBucketAcl($bucket){
        if(empty($bucket)){
            return "bucket not allowed empty";
        }
        return $this->client->getBucketAcl($bucket);
    }

    /** * * * * * * * * * * * * * * * * * * * * * *
     *  object options
     *
     *  object 的操作，object是oss的操作的基本单元
     *
     *
     * * * * * * * * * * * * * * * * * * * * * * * */

    #简单上传变量的内容到文件,force因为阿里云的写入机制，相同命名会被覆盖
    public function putObject($bucket,$file,$content,$force=true){
        if(empty($bucket) || empty($file) || empty($content)){
            return "param error";
        }

        $doseExist = $this->doesObjectExist($bucket,$file);

        if($force && $doseExist){
            return "file is already exist";
        }

        return $this->client->putObject($bucket,$file,$content);
    }

    #判断文件是否存在
    public function doesObjectExist($bucket,$file){
        if(empty($bucket) || empty($file)){
            return "param error";
        }
        return $this->client->doesObjectExist($bucket,$file);
    }

    /**
     * 上次本地文件
     *
     * @param $bucket
     * @param $file_name 上传后的文件名
     * @param $file 本地文件路径
     */
    public function uploadFile($bucket,$file_name,$file,$force = true){
        if(empty($bucket) || empty($file_name) || empty($file)){
            return "param error";
        }

        $doseExist = $this->doesObjectExist($bucket,$file_name);

        if($force && $doseExist){
            return "file is already exist";
        }

        return $this->client->uploadFile($bucket,$file_name,$file);
    }

    /**
     * 获取文件的内容到变量中
     *
     * @param $bucket
     * @param $object
     */
    public function getObject($bucket,$object){
        if(empty($bucket) || empty($object)){
            return "param error";
        }

        return $this->client->getObject($bucket,$object);
    }

    /**
     * 下载文件到本地
     *
     * @param $bucket
     * @param $object
     */
    public function downloadObject($bucket,$object,$saveFileName){

        if(empty($bucket) || empty($object) || empty($saveFileName)){
            return "param error";
        }

        $option = array(
            OssClient::OSS_FILE_DOWNLOAD => $saveFileName
        );

         return $this->client->getObject($bucket,$object,$option);
    }

    /**
     * 将bucket1中的file，复制一份到bucket2中
     *
     * @param $bucket1
     * @param $bucket_file
     * @param $bucket2
     * @param $bucket2_file
     */
    public function copyObject($fromBucket, $fromObject, $toBucket, $toObject){
        if(empty($fromBucket) || empty($fromObject) || empty($toBucket) || empty($toObject)){
            return "param error";
        }

        return $this->client->copyObject($fromBucket, $fromObject, $toBucket, $toObject);
    }

    /**
     * 删除object
     *
     * @param $bucket
     * @param $object
     */
    public function deleteObject($bucket,$object){
        if(empty($bucket) || empty($object)){
            return "param error";
        }

        return $this->client->deleteObject($bucket,$object);
    }

    // 批量删除object
    public function deleteObjects($bucket,array $objects){
        if(empty($bucket) || empty($objects) || !is_array($objects)){
            return "param error";
        }

        return $this->client->deleteObjects($bucket,$objects);
    }

    /** * * * * * * * * * * * * * * * * * * * * * *
     *  referer options
     *
     *  referer 的操作，referer是oss的允许访问的白名单
     *
     *  如果在bucket中设置了bucket的访问权限，设置referer的访问权限，将开发的域名
     *
     * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * 是否允许空的referer访问
     *
     * @param $status
     */
    public function setAllowEmptyReferer($bucket,$status=true){
        if(empty($bucket)){
            return "param error";
        }

        $refererConfig = new RefererConfig();
        $refererConfig->setAllowEmptyReferer($status);

       return $this->client->putBucketReferer($bucket,$refererConfig);
    }

    /**
     * 添加bucket的包名单，一般不要使用
     *
     * @param $bucket
     * @param array $domain
     * @return Http\ResponseCore|string
     */
    public function addBucketReferer($bucket,array $domain){
        if(!is_array($domain) || empty($bucket)){
            return "param error";
        }
        $refererConfig = new RefererConfig();

        foreach ($domain as $item){
            if(preg_match("/[0-9a-zA-Z]+[0-9a-zA-Z\.-]*\.[a-zA-Z]{2,4}/",$item)){
                $refererConfig->addReferer($item);
            }else{
                return "domain url Error";
            }
        }

        return $this->client->putBucketReferer($bucket,$refererConfig);
    }

    /**
     *  获取bucket的referer白名单
     *
     * @param $bucket
     * @return string
     */
    public function getBucketReferer($bucket){
        if(empty($bucket)){
            return "param error";
        }

        $exist = $this->doesBucketExist($bucket);

        if($exist){
            return $this->client->getBucketReferer($bucket)->getRefererList();
        }else{
            return "The bucket is not exist";
        }
    }

    /** * * * * * * * * * * * * * * * * * * * * * *
     *  url 相关 options
     *
     *  将文件生产url，可以通过这个url上传或者下载文件
     *
     *
     *
     * * * * * * * * * * * * * * * * * * * * * * * */

    /**
     * 生成GetObject的签名url，用户可以使用这个url直接在浏览器下载
     *
     * @param $bucket
     * @param $fileName
     * @param $expire
     * 生产结果
     * http://xiaoyutech-vuxmcm.oss-cn-beijing.aliyuncs.com/0c924db708b5d397741aa4329648d982.copy?OSSAccessKeyId=cW5oi2442ZGRnF2z&Expires=1471842381&Signature=WEWhravuIwzGr9KNsXyUitKRAsg%3D
     *
     * 这里会查收一个下载链接
     */
    public function signUrl($bucket,$fileName,$expire = 3600,$option = "GET"){
        if(empty($bucket) || empty($fileName)){
            return "param error";
        }

        $option = strtolower($option);

        if($option === 'get'){
            return $this->client->signUrl($bucket,$fileName,$expire);
        }else{
            return $this->client->signUrl($bucket,$fileName,$expire,'PUT');
        }
    }

    // 生成从本地文件上传PutObject的签名url, 用户可以直接使用这个url把本地文件上传到　"a.file"
    /**
     * 目前感觉没有什么用武之地,并且阿里云的这样调用存在问题
     *
     * @param $bucket
     * @param $fileName
     * @param int $expire
     * @return string
     * @throws OssException
     */
    public function uploadSignUrl($bucket,$fileName,$expire = 3600){
        if(empty($bucket) || empty($fileName)){
            return "param error";
        }

        return $this->client->signUrl($bucket,$fileName,$expire,'PUT',array("'Content-Type' => 'txt'"));
    }

    
    /* end of client */
}
