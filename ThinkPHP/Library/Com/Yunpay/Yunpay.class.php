<?php
namespace Com\Yunpay;

class Yunpay{
    protected $i2ekeys='';

    /**
     * 构造函数,
     *
     * Yunpay constructor.
     */
    public function __construct()
    {
        if(empty(C('yun_pay_keys'))){
            E('未设置yunpaykeys');
        }
        $this->i2ekeys = C('yun_pay_keys');
    }

//     参数格式
//    $parameter = array(
//            "partner" => trim($yun_config['partner']),
//            "seller_email"	=> $seller_email,
//            "out_trade_no"	=> $out_trade_no,
//            "subject"	=> $subject,
//            "total_fee"	=> $total_fee,
//            "body"	=> $body,
//            "nourl"	=> $nourl,
//            "reurl"	=> $reurl,
//            "orurl"	=> $orurl,
//            "orimg"	=> $orimg
//    );


    /**
     * yunpay 云支付的函数
     *
     * @param $parameter
     * @param string $msg
     * @return string
     */
    public function pay($parameter,$msg='支付进行中...'){
        $myparameter = '';

        foreach ($parameter as $pars) {
            $myparameter.=$pars;
        }
        $sign=md5($myparameter.'i2eapi'.$this->i2ekeys);
        $str="<form name='yunsubmit' action='http://pay.yunpay.net.cn/i2eorder/yunpay/' accept-charset='utf-8' method='get'><input type='hidden' name='body' value='".$parameter['body']."'/><input type='hidden' name='out_trade_no' value='".$parameter['out_trade_no']."'/><input type='hidden' name='partner' value='".$parameter['partner']."'/><input type='hidden' name='seller_email' value='".$parameter['seller_email']."'/><input type='hidden' name='subject' value='".$parameter['subject']."'/><input type='hidden' name='total_fee' value='".$parameter['total_fee']."'/><input type='hidden' name='nourl' value='".$parameter['nourl']."'/><input type='hidden' name='reurl' value='".$parameter['reurl']."'/><input type='hidden' name='orurl' value='".$parameter['orurl']."'/><input type='hidden' name='orimg' value='".$parameter['orimg']."'/><input type='hidden' name='sign' value='".$sign."'/></form><script>document.forms['yunsubmit'].submit();</script>";

        return $str;
    }

    /**
     * 签名验证函数
     *
     * @param $i1
     * @param $i2
     * @param $i3
     * @param $key
     * @param $pid
     * @return bool
     */
    public function md5Verify($i1, $i2,$i3,$key,$pid) {
        $prestr = $i1 . $i2.$pid.$key;
        $mysgin = md5($prestr);

        if($mysgin == $i3) {
            return true;
        }
        else {
            return false;
        }
    }

    /*end of yun pay class*/
}