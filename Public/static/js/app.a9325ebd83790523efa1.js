webpackJsonp(
    [20],
    {124:function(e,n,t){"use strict";
     Object.defineProperty(n,"__esModule",{value:!0});
        var a=t(30),r=function(e){
            return a.a.post("/public/auth?include=user",{type:"wechat",code:e})
        };
        (function(e){function a(n,t){
            var a="",r="";
            if("wechat"===n)a="wx60d87b733b2005c9",r="appwx";
            else{if("super"!==n)return void e.alert("请在微信浏览器中打开");
                a="N5rUNozQ7ggwjHXOVQpK",r="supercn"}
            var o=location.hash.split("?"),i=h.a.parse(o[1]);
            delete i.code,delete i.state;
            var c=""+location.origin+location.pathname+"?"+o[0];
            c=t?location.origin+"/?#"+t.fullPath:c,!t&&p()(i).length>0&&(c+="?"+h.a.stringify(i));
            var u="http://www.mianbaolive.com/h5/auth.html?appid="
                +a+"&state="+r+"&backurl="
                +encodeURIComponent(c);
            location.replace(u)
        }
            var o=t(28),i=t.n(o),c=t(29),u=t.n(c),s=t(125),p=t.n(s),l=t(129),h=t.n(l),d=t(17),f=t(132),m=t(16),v=t(15),b=t(55),w=this;
            d.a.use(f.a);
            var g=new f.a({routes:[
                {path:"/albums/:id",
                component:function(e){
                    return t.e(10).then(function(){var n=[t(292)];
                        e.apply(null,n)}.bind(this)).catch(t.oe)}
                },
                {path:"/series-course/:id",
                 name:"seriesCourse",
                 meta:{title:"课程详情"},
                 component:function(e){
                      return t.e(2).then(function(){
                          var n=[t(293)];e.apply(null,n)
                      }.bind(this)).catch(t.oe)
                    }
                },
                {path:"/course-detail/:id",name:"liveDetail",
                 component:function(e){
                     return t.e(3).then(function(){var n=[t(294)];
                         e.apply(null,n)}.bind(this)).catch(t.oe)
                 }
                },
                {name:"coursePaymentResult",
                 path:"/courses/:id/payment-result",
                 component:function(e){
                     return t.e(0).then(function(){
                         var n=[t(295)];
                         e.apply(null,n)}.bind(this)).catch(t.oe)
                 }
                },
                {path:"/courses/:id/chatroom",
                 component:function(e){
                     return t.e(9).then(function(){
                         var n=[t(296)];e.apply(null,n)}.bind(this)).catch(t.oe)
                 }
                },
                {path:"/paid-success",
                 name:"paid-success",
                 component:function(e){
                     return t.e(0).then(function(){
                         var n=[t(295)];e.apply(null,n)
                     }.bind(this)).catch(t.oe)
                 }
                },
                {path:"/user-purchased-course",
                 name:"userPurchasedCourse",
                 component:function(e){
                     return t.e(6).then(function(){var n=[t(297)];
                         e.apply(null,n)}.bind(this)).catch(t.oe)
                 }
                },
                {path:"/user-purchased-course-all",
                    name:"userPurchasedCourseAll",
                    component:function(e){return t.e(5).then(function(){
                        var n=[t(298)];e.apply(null,n)}.bind(this)).catch(t.oe)
                    }
                },
                {path:"/audio-course/:id",
                 name:"audioCourse",
                 meta:{title:"小课播放"},
                 component:function(e){
                     return t.e(1).then(function(){
                         var n=[t(299)];
                         e.apply(null,n)}.bind(this)).catch(t.oe)
                     }
                },
                {path:"/retailer/:id/course-list",
                    name:"retailerCourseList",
                    component:function(e){
                        return t.e(8).then(function(){
                            var n=[t(300)];e.apply(null,n)}.bind(this)).catch(t.oe)
                    }
                },
                {path:"/retailer/:id/userpage",
                    name:"userPage",
                    component:function(e){
                        return t.e(7).then(function(){var n=[t(301)];
                            e.apply(null,n)}.bind(this)).catch(t.oe)
                    }
                },
                {path:"/retailer/:id/purchased",
                    name:"userPurchasedCourseInRetailer",
                    component:function(e){return t.e(4).then(function(){
                        var n=[t(302)];e.apply(null,n)}.bind(this)).catch(t.oe)
                    }
                },
                {path:"/smallShops/:id",
                    component:function(e){
                        return t.e(16).then(function(){
                            var n=[t(303)];
                            e.apply(null,n)
                        }.bind(this)).catch(t.oe)
                    },
                 children:[{path:"home",
                 component:function(e){
                     return t.e(17).then(function(){
                         var n=[t(304)];e.apply(null,n)
                     }.bind(this)).catch(t.oe)
                 }
                },
                {path:"purchaseList",
                    component:function(e){return t.e(15).then(function(){var n=[t(305)];e.apply(null,n)}.bind(this)).catch(t.oe)}},{path:"userCenter",component:function(e){return t.e(14).then(function(){var n=[t(306)];e.apply(null,n)}.bind(this)).catch(t.oe)}}]},{path:"/business-users/:id/bindWechat",name:"businessUserBindWechat",component:function(e){return t.e(18).then(function(){var n=[t(307)];e.apply(null,n)}.bind(this)).catch(t.oe)}},{path:"/checkSeriesQRCode",component:function(e){return t.e(12).then(function(){var n=[t(308)];e.apply(null,n)}.bind(this)).catch(t.oe)}},{path:"/group-fission/:id",name:"groupFission",component:function(e){return t.e(11).then(function(){var n=[t(309)];e.apply(null,n)}.bind(this)).catch(t.oe)}},{path:"*",name:"404",component:function(e){return t.e(13).then(function(){var n=[t(310)];e.apply(null,n)}.bind(this)).catch(t.oe)}}]});g.beforeEach(function(e,n,t){var a=!1;if("groupFission"!==e.name)try{var r=m.a.getters.token;if(r._id){var o=+new Date,i=r.created_at;a=i+36e5>o}else a=!1}catch(e){a=!1}else a=!0;g.isAuth=a,t()}),g.beforeEach(function(){var e=u()(i.a.mark(function e(n,t,o){var c,u,s,p,l,d,f,b,y;return i.a.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:if(c=n.query,u=c.code,s=c.state,!(p=g.isAuth)){e.next=7;break}return v.a.quick("autoTrack",{$url_path:n.path}),o(),e.abrupt("return");case 7:if(l="",d=window.navigator.userAgent.toLocaleLowerCase(),d.match(/micromessenger/)?l="wechat":d.match(/superfriday/)&&(l="super"),!u||!s){e.next=29;break}return e.prev=11,e.next=14,r(u);case 14:f=e.sent,b=f.user,delete f.user,y=f,m.a.dispatch("setUser",b),m.a.dispatch("setToken",y),delete c.code,delete c.state,o(n.path+"?"+h.a.stringify(c)),e.next=28;break;case 25:e.prev=25,e.t0=e.catch(11),a("wechat",n);case 28:return e.abrupt("return");case 29:"businessUserBindWechat"===n.name&&o(),a(l,n);case 31:case"end":return e.stop()}},e,w,[[11,25]])}));return function(n,t,a){return e.apply(this,arguments)}}()),g.afterEach(function(e,n){var t=e.meta.title||"面包小课";Object(b.a)(t)}),n.a=g}).call(n,t(139))},15:function(e,n,t){"use strict";var a=t(117),r=t.n(a),o=location,i=o.protocol,c="https:"===i?4006:8006,u=new URL("http://paidian.cloud.sensorsdata.cn:8006/sa?token=8a1b505f7ffcf481&project=production");u.protocol=i,u.port=c,r.a.init({server_url:u.toString()}),n.a=r.a},16:function(e,n,t){"use strict";Object.defineProperty(n,"__esModule",{value:!0});var a,r=t(118),o=t.n(r),i=t(122),c=t.n(i),u=t(15),s=(a={},o()(a,"SET_USER",function(e,n){e.user=n,u.a.login(n.id),localStorage.setItem("bl_user",c()(n))}),o()(a,"SET_TOKEN",function(e,n){e.token=n,localStorage.setItem("bl_token",c()(n))}),a),p={setUser:function(e,n){(0,e.commit)("SET_USER",n)},setToken:function(e,n){(0,e.commit)("SET_TOKEN",n)}},l={user:function(e){return e.user},token:function(e){return e.token}},h=t(17),d=t(53),f=t(15);h.a.use(d.a);var m=void 0,v=void 0;try{m=JSON.parse(localStorage.getItem("bl_user"))||{},v=JSON.parse(localStorage.getItem("bl_token"))||{}}catch(e){m={},v={}}m.id&&f.a.login(m.id);var b={user:m,token:v};n.a=new d.a.Store({state:b,mutations:s,actions:p,getters:l})},30:function(e,n,t){"use strict";var a=t(52),r=t.n(a),o=t(16),i=r.a.create({baseURL:"//api.mianbaolive.com/v1",timeout:1e4});i.interceptors.request.use(function(e){return e.headers.Authorization=o.a.getters.token._id,e}),i.interceptors.response.use(function(e){return e.data}),n.a=i},55:function(e,n,t){"use strict";t.d(n,"b",function(){return a}),t.d(n,"a",function(){return r});var a=function(e){var n={title:e.title,desc:e.desc,link:e.link,img_url:e.image_url,imgUrl:e.image_url};window.wx?window.wx.ready(function(){window.wx.onMenuShareTimeline(n),window.wx.onMenuShareAppMessage(n)}):document.addEventListener("WeixinJSBridgeReady",function(e){window.wx&&window.wx.ready(function(){window.wx.onMenuShareTimeline(n),window.wx.onMenuShareAppMessage(n)})})},r=function(e){document.title=e;var n=document.body,t=document.createElement("iframe");t.style.display="none",t.setAttribute("src","/static/load.png"),t.addEventListener("load",function(){t.removeEventListener("load",!1),n.removeChild(t)}),n.appendChild(t)}},59:function(e,n,t){"use strict";t.d(n,"a",function(){return r});var a=t(30),r=function(e){return a.a.get("/wechat/config?url="+encodeURIComponent(e))}},60:function(e,n,t){"use strict";function a(e){t(62)}Object.defineProperty(n,"__esModule",{value:!0});var r=t(28),o=t.n(r),i=t(54),c=t.n(i),u=t(29),s=t.n(u),p=t(59),l={name:"app",created:function(){},mounted:function(){var e=this;return s()(o.a.mark(function n(){var t,a,r,i,u;return o.a.wrap(function(e){for(;;)switch(e.prev=e.next){case 0:return t=window,a=t.wx,r=t.location,i=r.href.split("#")[0],e.next=4,Object(p.a)(i);case 4:u=e.sent,a.config(c()({},u,{url:window.encodeURIComponent(i),jsApiList:["chooseWXPay","onMenuShareTimeline","onMenuShareAppMessage"]}));case 6:case"end":return e.stop()}},n,e)}))()}},h=function(){var e=this,n=e.$createElement,t=e._self._c||n;return t("div",{attrs:{id:"app"}},[t("router-view",{staticClass:"page"})],1)},d=[],f={render:h,staticRenderFns:d},m=f,v=t(50),b=a,w=v(l,m,b,null,null),g=w.exports,y=navigator.userAgent,_=/(Android);?[\s\/]+([\d.])?/.test(y),S=/(iPad).*OS\s([\d_]+)/.test(y),k=/(iPod)(.*OS\s([\d_]+))?/.test(y),x=!k&&/(iPhone\sOS)\s([\d_]+)/.test(y),E=/micromessenger/i.test(y),C=/weibo/i.test(y),A=function(e){e.mixin({created:function(){this.$device={isAndroid:_,isIpad:S,isIpod:k,isIphone:x,isWechat:E,isWeibo:C}}})},O=t(17),P=t(61),T=t.n(P),U=t(16),M=t(124);t(15);O.a.use(A),O.a.config.productionTip=!1,T.a.attach(document.body),new O.a({store:U.a,router:M.a,render:function(e){return e(g)}}).$mount("#app")},62:function(e,n){}},[60]);
//# sourceMappingURL=app.a9325ebd83790523efa1.js.map