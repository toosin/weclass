<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2014 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用入口文件

// 检测PHP环境
if (version_compare(PHP_VERSION, '5.3.0', '<')) {
    die('require PHP > 5.3.0 !');
}

//调试模式是否开启
$debug = isset($_GET['debug']) ? true : false;
define('APP_DEBUG', $debug);

// NGINX环境变量TP_ENV , 定义APP_STATUS应用状态
if (isset($_SERVER['TP_ENV'])) {
    $tp_env = strtolower(trim($_SERVER['TP_ENV']));
    define('APP_STATUS', $tp_env);
}


// 定义应用目录
define('APP_PATH', '../Application/');
// 定义程序根目录
define("APP_ROOT", dirname(dirname(__FILE__)));
// 定义访问入口路径
define("APP_PUBLIC_PATH", dirname(__FILE__));


// 定义runtime文件夹
if (defined('APP_STATUS')) {
    define('RUNTIME_PATH', APP_PATH . 'Runtime_' . APP_STATUS . '/');
}

// 引入ThinkPHP入口文件
require '../ThinkPHP/ThinkPHP.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单
