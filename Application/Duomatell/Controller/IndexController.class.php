<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/11/3 0003
 * Time: 下午 17:12
 */
namespace Duomatell\Controller;
use Common\Model\CategoryModel;
use Common\Model\ClassModel;
use Common\Model\SpecialModel;
use Duomatell\Model\UserModel;
use Think\Controller;

class IndexController extends Controller{
    public function index(){
        echo 'ggg';
    }
    public function getMenu(){
        $special    =   new SpecialModel();
        $category   = new CategoryModel();
        $filter =   ['status'=>1,'is_show'=>1];
        $result =   array();
        $category_info  =   $category->where($filter)->select();
        foreach($category_info as $key=>$value){
            $specialinfo   =   $special->where(['cid'=>$value['id'],'status'=>1])->limit(2)->select();
            if($specialinfo[0]){
                $specialinfo[0]['poster_s']=C('CDN_URL').'/'.$specialinfo[0]['poster_s'];
                $specialinfo[0]['poster_c']=C('CDN_URL').'/'.$specialinfo[0]['poster_c'];
                $specialinfo[1]['poster_s']=C('CDN_URL').'/'.$specialinfo[1]['poster_s'];
                $specialinfo[1]['poster_c']=C('CDN_URL').'/'.$specialinfo[1]['poster_c'];
                $result[]=array(
                    'cat_id'=>$value['id'],
                    'icon'=>C('CDN_URL').'/'.$value['icon'],
                    'cat_name'=>$value['title'],
                    'special'=>$specialinfo
                );
            }
        }
        $this->ajaxReturn($result);
    }

    public function getSpecialItem(){
        if($key =   I('get.id')){
            $special_model=new SpecialModel();
            $filter =   array(
                'id'=>$key,
            );
            $special_info=$special_model->where($filter)->find();
            $special_info['poster_s']=C('CDN_URL').'/'.$special_info['poster_s'];
            $special_info['poster_c']=C('CDN_URL').'/'.$special_info['poster_c'];
            $userModel  =   M('user');
            $uid    =   $userModel->where(['openid'=>I('get.openid')])->getField('id');
            $collectionModel    =   M('collection');
            $collectionId=$collectionModel->where(['uid'=>$uid,'sid'=>$special_info['id']])->getField('id');
            $special_info['is_collection']=$collectionId?1:0;
            $classModel =   new ClassModel();
            $class_info =   $classModel->where(['sid'=>$key,'status'=>1])->select();
            foreach($class_info as $key=>$value){
                $class_info[$key]['poster_c']=C('CDN_URL').'/'.$class_info[$key]['poster_c'];
                $class_info[$key]['poster_s']=C('CDN_URL').'/'.$class_info[$key]['poster_s'];
                $class_info[$key]['audio']=C('CDN_URL').'/'.$class_info[$key]['audio'];
            }
            $result=array(
                'special_info'=>$special_info,
                'class_info'=>$class_info
            );
            $this->ajaxReturn($result);
        }
    }

    public function getClassItem(){
        if($key =   I('get.key'))
        {
            $classModel =   new ClassModel();
            $class_info =   $classModel->where('id='.$key)->find();
            $class_info['audio']=C('CDN_URL').$class_info['audio'];
            $class_info['poster_c']=C('CDN_URL').$class_info['poster_c'];
            $class_info['poster_s']=C('CDN_URL').$class_info['poster_s'];
            $userModel  =   M('user');
            $uid    =   $userModel->where(['openid'=>I('get.openid')])->getField('id');
            $collectionModel    =   M('collection');
            $collectionId=$collectionModel->where(['uid'=>$uid,'cid'=>$class_info['id'],'status'=>1])->getField('id');
            $class_info['is_collection']=$collectionId?1:0;
            $this->ajaxReturn($class_info);
        }
    }

    public function getCategoryDetail(){
        $category   = new CategoryModel();
        $category_info  =   $category->where('status=1')->select();
        $this->ajaxReturn($category_info);
    }

    public function getCatClass(){
        if(I('get.cid')){
            $classModel  =   new SpecialModel();
            $specialInfo  =   $classModel->where(['cid'=>I('get.cid'),'status'=>1])->select();
            foreach($specialInfo as $key=>$value)
            {
                $specialInfo[$key]['poster_c']=C('CDN_URL').$specialInfo[$key]['poster_c'];
            }
            $this->ajaxReturn($specialInfo);
        }
    }

    public function getUserInfo(){
        if($code=I('code')){
            $res=@file_get_contents("https://api.weixin.qq.com/sns/jscode2session?appid=wx584ab294ba90975f&secret=2f2952c9767b6a64fe06cd957fc66a9e&js_code=$code&grant_type=authorization_code");
            $res=json_decode($res,true);
            $final  =   array('openid'=>$res['openid']);
            try{
                $userModel  = M('user');
                $userinfo=$userModel->where(['openid'=>$res['openid']])->find();
                if($userinfo['id'])
                {
                    $data   =   array(
                        'openid'=>$res['openid'],
                        'updatetime'=>date('Y-m-d H:i:s')
                    );
                    $userModel->where(['id'=>$userinfo['id']])->save($data);
                }else
                {
                    $data   =   array(
                        'openid'=>$res['openid'],
                        'status'=>1,
                        'addtime'=>date('Y-m-d H:i:s')
                    );
                    $userModel->data($data)->add();
                }
            }catch(\Exception $e)
            {
                my_log($e->getMessage());
            }
            $this->ajaxReturn($final);
        }
    }

    public function setCollection(){
        if(($key=I('get.key'))&&($openid=I('get.openid'))){

            $userModel  =   M('user');
            $id=$userModel->where(['openid'=>$openid])->getField('id');
            $collection =   M('collection');
            $data   =   array(
                'uid'=>$id,
                'sid'=>$key,
                'type'=>1,
                //'addtime'=>date('Y-m-d H:i:s'),
                'status'=>1
            );
            $collection->where($data)->find();
            if(!$collection['id']){
                $data['addtime']=date('Y-m-d H:i:s');
                $collection->data($data)->add();
            }
        }
    }
    public function getCollection(){
        if($openid=I('get.openid')){
            $userModel  =   M('user');
            $id=$userModel->where(['openid'=>$openid])->getField('id');
            $collection =   M('collection');
            $collection_info=$collection->where(['uid'=>$id,'status'=>1])->select();
            $specialModel   =   new SpecialModel();
            $res=array();
            foreach($collection_info as $data){
                $special_info   =   $specialModel->where(['id'=>$data['sid'],'status'=>1])->find();
                $special_info['poster_s']=C('CDN_URL').$special_info['poster_s'];
                $res[]=$special_info;
            }
            $this->ajaxReturn($res);
        }
    }

    public function setHistory(){
        $historyModel=M('history');
        $userModel  =   M('user');
        if(I('get.openid')&&I('get.cid')){
            $userid=$userModel->where(['openid'=>I('get.openid')])->getField('id');
            if($userid){
                $filter  = array(
                  'uid'=> $userid,
                  'cid'=>I('get.cid'),
                  'status'=>1
                );
                $id = $historyModel->where($filter)->getField('id');
                $filter['addtime']=date('Y-m-d H:i:s');
                if($id){
                    $historyModel->where(['id'=>$id])->save($filter);
                }else{
                    $historyModel->data($filter)->add();
                }
            }
        }
    }

    public function getHistory(){
        $historyModel = M('history');
        if(I('get.openid'))
        {
            $userModel  =   M('user');
            $userid=$userModel->where(['openid'=>I('get.openid')])->getField('id');
            $history=$historyModel->where(['uid'=>$userid])->limit(1)->select();
            $classModel =   new ClassModel();
            $class_info =   array();
            foreach ($history as $item) {
                $class_res=$classModel->where(['cid'=>$item['cid'],'status'=>1])->find();
                $class_res['play_time']=$item['addtime'];
                $class_res['poster_s']=C('CDN_URL').$class_res['poster_s'];

                $class_res['title']=mb_strlen($class_res['title'])>5?mb_substr($class_res['title'],0,5).'..':$class_res['title'];
                $class_info[]=$class_res;
            }
            $this->ajaxReturn($class_info);
        }
    }
    public function getHistoryAll(){
        $historyModel = M('history');
        if(I('get.openid'))
        {
            $userModel  =   M('user');
            $userid=$userModel->where(['openid'=>I('get.openid')])->getField('id');
            $history=$historyModel->where(['uid'=>$userid])->limit(20)->select();
            $classModel =   new ClassModel();
            $class_info =   array();
            foreach ($history as $item) {
                $class_res=$classModel->where(['cid'=>$item['cid'],'status'=>1])->find();
                $class_res['play_time']=$item['addtime'];
                $class_res['poster_c']=C('CDN_URL').$class_res['poster_c'];

                //$class_res['title']=mb_strlen($class_res['title'])>5?mb_substr($class_res['title'],0,5).'..':$class_res['title'];
                $class_info[]=$class_res;
            }
            $this->ajaxReturn($class_info);
        }
    }
    public function getCollectionOne(){
        if($openid=I('get.openid')){
            $userModel  =   M('user');
            $id=$userModel->where(['openid'=>$openid])->getField('id');
            $collection =   M('collection');
            $collection_info=$collection->where(['uid'=>$id,'status'=>1])->limit(1)->select();
            $total  =   $collection->where(['uid'=>$id,'status'=>1])->count('id');
            $specialModel   =   new SpecialModel();
            $res=array();
            foreach($collection_info as $data){
                $special_info   =   $specialModel->where(['id'=>$data['sid'],'status'=>1])->find();
                $special_info['poster_s']=C('CDN_URL').$special_info['poster_s'];
                $special_info['total']=$total;
                $res[]=$special_info;
            }
            $this->ajaxReturn($res);
        }
    }

    public function getCollectionAll(){
        if($openid=I('get.openid')){
            $userModel  =   M('user');
            $id=$userModel->where(['openid'=>$openid])->getField('id');
            $collection =   M('collection');
            $collection_info=$collection->where(['type'=>1,'uid'=>$id,'status'=>1])->select();
            $specialModel   =   new SpecialModel();
            $res=array();
            foreach($collection_info as $data){
                $special_info   =   $specialModel->where(['id'=>$data['sid'],'status'=>1])->find();
                $special_info['poster_c']=C('CDN_URL').$special_info['poster_c'];
                $res['special'][]=$special_info;
            }
            $collection_info2=$collection->where(['type'=>2,'uid'=>$id,'status'=>1])->select();
            $classModel =   new ClassModel();
            foreach($collection_info2 as $data){
                $class_info   =  $classModel->where(['id'=>$data['cid'],'status'=>1])->find();
                $class_info['poster_c']=C('CDN_URL').$special_info['poster_c'];
                $res['class'][]=$special_info;
            }
            $this->ajaxReturn($res);
        }
    }

    public function getNextClassItem(){
        if($key =   I('get.cid'))
        {
            $classModel =   new ClassModel();
            $class_info =   $classModel->where('id='.$key)->find();
            if(I('get.type')=='inc'){
                $class_info   =   $classModel->where(['num'=>++$class_info['num']])->find();
                $class_info['audio']=C('CDN_URL').$class_info['audio'];
                $class_info['poster_c']=C('CDN_URL').$class_info['poster_c'];
                $class_info['poster_s']=C('CDN_URL').$class_info['poster_s'];
            }else{
                $class_info  =   $classModel->where(['num'=>--$class_info['num']])->find();
                $class_info['audio']=C('CDN_URL').$class_info['audio'];
                $class_info['poster_c']=C('CDN_URL').$class_info['poster_c'];
                $class_info['poster_s']=C('CDN_URL').$class_info['poster_s'];
            }
            $this->ajaxReturn($class_info);
        }
    }

    public function getNav()
    {
        $CategoryModel   =  new CategoryModel();
        $nav    =   $CategoryModel->where(['status'=>1,'is_show_top'=>1])->limit(4)->select();
        foreach($nav as $key=>$value){
            $nav[$key]['top_icon']  =   C('CDN_URL').$nav[$key]['top_icon'];
        }
        $this->ajaxReturn($nav);
    }

    public function setCollectionClass(){
        if($openid=I('get.openid')){
            $userModel  =   M('user');
            $id=$userModel->where(['openid'=>$openid])->getField('id');
            $collection =   M('collection');
            $collection_info=$collection->where(['uid'=>$id,'cid'=>I('get.cid')])->find();
            if(!$collection_info['id'])
            {
                $data   =   array(
                  'uid'=>$id,
                  'cid'=>I('get.cid'),
                  'status'=>1,
                  'addtime'=>date('Y-m-d H:i:s'),
                    'type'=>2
                );
                $collection->data($data)->add();
            }else
            {
                if($collection_info['status']==1){
                    $collection->where(['uid'=>$id,'cid'=>I('get.cid')])->save(['status'=>0]);
                }else{
                    $collection->where(['uid'=>$id,'cid'=>I('get.cid')])->save(['status'=>1]);
                }
            }

        }
    }
}