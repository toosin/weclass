<?php
/**
 * 个人中心
 */
namespace Demo\Controller;

/**
 * 举例:需要登录后才能访问的业务逻辑，只需继承 ControllerBase
 * @author i
 *
 */
class MyController extends ControllerBase
{

    /**
     * 首页
     */
    public function index()
    {

        echo "你的用户信息：";
        var_dump($this->userInfo);
    }
    
    
}