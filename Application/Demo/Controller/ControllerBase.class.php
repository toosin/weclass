<?php
namespace Demo\Controller;

use Think\Controller;
use Common\Model\ConfigModel as Config;
use Common\Model\OpenIdModel as OpenId;
use Common\Model\UserModel as User;

class ControllerBase extends Controller
{

    protected $userInfo = NULL;

    protected $uid = NULL;

    protected $openid = NULL;

    public function __construct()
    {
        parent::__construct();
        
        $uid = I('uid');
        if ($uid)
        {
            $uid = intval($uid);
            $user_obj = new User();
            $where = array();
            $where['id'] = $uid;
            $user_info = $user_obj->where($where)->find();
            if ($user_info)
            {
                $this->userInfo = $user_info;
                $this->uid = $user_info['id'];
                $this->openid = '';
                
                $config_obj = new Config();
                $read_only = $config_obj->getConfig('READ_ONLY');
                
                if ($read_only == 1)
                {
                    // 伪登录开启DB只读模式
                    session('DB_READ_ONLY', 1);
                }
                
                session('uid', $this->uid);
                session('openid', '');
                session('userInfo', $user_info);
            }
        }
        
        try
        {
            $userInfo = session('userInfo');
            
            $openid = I('openid');
            
            if ($openid)
            {
                session('[destroy]');
            }
            
            if (empty($userInfo) || $openid)
            {
                
                if (IS_AJAX)
                {
                    E("超时了，请返回微信界面，点击菜单重新开始!", 401);
                }
                
                if ($openid)
                {
                    
                    $u = new User();
                    $open_obj = new OpenId();
                    
                    $uid = $open_obj->openid2Uid($openid);
                    
                    $where = array();
                    $where['id'] = $uid;
                    $user_info = $u->where($where)->find();
                    
                    if (empty($user_info))
                    {
                        E('获取用户信息失败');
                    }
                    
                    session('uid', $uid);
                    $this->uid = $uid;
                    $this->openid = $openid;
                    session('openid', $openid);
                    $this->userInfo = $user_info;
                    session('userInfo', $user_info);
                } else
                {
                    $config = new Config();
                    $auth_mode = $config->getConfig("AUTH_MODE");
                    $auth_mode = $auth_mode ? $auth_mode : C('AUTH_MODE');
                    // 网页授权
                    if ($auth_mode == 1)
                    {
                        $url = 'http://' . C('MAIN_DOMAIN') . $_SERVER["REQUEST_URI"];
                        session('auth_callback_url', $url);
                        redirect(U("Auth/getinfo"));
                        die();
                    } else
                    {
                        redirect(U("Error/authfail"));
                        die();
                    }
                }
            } else
            {
                $this->uid = session('uid');
                $this->openid = session('openid');
                $this->userInfo = session('userInfo');
            }
        } catch (\Exception $e)
        {
            echo $e->getMessage();
            die();
            if (IS_AJAX)
            {
                $ret = array();
                $ret['status'] = $e->getCode();
                $ret['msg'] = $e->getMessage();
                $ret['content'] = array();
                $this->ajaxReturn($ret);
            } else
            {
                redirect(U("Error/authfail"));
                die();
            }
        }
    }

    public function _empty()
    {
        if (IS_AJAX)
        {
            $data = array();
            $data['status'] = 404;
            $data['msg'] = 'action not found';
            
            echo json_encode($data);
        } else{
            echo "<!--empty action , i'm from " . MODULE_NAME . "~" . APP_STATUS, "-->";
        }
    }
    
}