<?php
namespace Demo\Widget;

use Think\Controller;
use Com\Weixin\WeixinJs;
use Common\Model\ConfigModel as Config;

class WeixinWidget extends Controller
{
    // 微信分享JS组件
    public function share()
    {
        $config_obj = new Config();
        $keys = array(
            'SHARE_TITLE',
            'SHARE_DESC',
            'SHARE_IMG',
            'SHARE_APPID',
            'SHARE_APPSECRET',
            'SHARE_URL'
        );
        $share_data = $config_obj->getConfigs($keys);
        
        $appid = $share_data['SHARE_APPID'];
        $appsecret = $share_data['SHARE_APPSECRET'];
        
        $weixin_js_obj = new WeixinJs($appid, $appsecret);
        $js_param = $weixin_js_obj->getSignPackage(false, 'js_share');
        
        $this->assign('signPackage', $js_param);
        $this->assign('share_data', $share_data);
        $this->assign('AppID', $appid ? $appid : C('AppID'));
        
        $this->display('Public:share');
    }
    
    // 微信分享JS组件,禁用分享
    public function noshare()
    {
         
        $appid = '';
        $appsecret = '';
    
        $weixin_js_obj = new WeixinJs($appid, $appsecret);
        $js_param = $weixin_js_obj->getSignPackage(false, 'js_share');
    
        $this->assign('signPackage', $js_param);
        $this->assign('AppID', $appid ? $appid : C('AppID'));
    
        $this->display('Public:noshare');
    }
}