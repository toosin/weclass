<?php
namespace Weclass1\Controller;

use Think\Controller;

class EmptyController extends Controller
{

    public function _empty()
    {
        if (IS_AJAX)
        {
            $data = array();
            $data['status'] = 404;
            $data['msg'] = 'controller not found';
            
            echo json_encode($data);
        } else
        {
            echo "<!--empty controller , i'm from " . MODULE_NAME . "~" . APP_STATUS, "-->";
        }
    }
}