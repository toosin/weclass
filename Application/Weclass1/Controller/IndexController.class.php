<?php
namespace Weclass1\Controller;

use Com\Weixin\WeixinJs;
use Think\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $this->display('Index/empty');
        //echo "<!-- Module name:".MODULE_NAME.",Controller name:".CONTROLLER_NAME."-->";
    }
    public function article(){
        if(I('get.id')){
            $wxjs   =   new  WeixinJs(C('AppID'),C('AppSecret'));
            $signPakg=$wxjs->getSignPackage(true);

            $this->assign('appid',C('AppID'));

            $this->assign('config',$signPakg);
            $classModel = M('class');
            $class=$classModel->where(['id'=>base64_decode(I('get.id')),'status'=>1])->find();
            if($class['id'])
            {
                $class['audio']="/weclass_static/audio/".$class['audio'];
                $class['share_img']="/weclass_static/img/".$class['share_img'];
                $class['top_back']="/weclass_static/img/".$class['top_back'];
                $class['middle_desc_img']="/weclass_static/img/".$class['middle_desc_img'];
                $class['usage_img']="/weclass_static/img/".$class['usage_img'];
                $class['group_num']=$class['group_num']>=100000?"100000+":$class['group_num'];
                $this->assign('class',$class);
                $this->display('class');
            }else
            {
                echo "课程不存在！";
            }

        }else{
            echo "课程不存在！";
        }
    }
    public function test(){
        $wxjs   =   new  WeixinJs(C('AppID'),C('AppSecret'));
        $signPakg=$wxjs->getSignPackage(true);
        // $domain  =  array('http://www.sznfdx.com.cn/focus','http://www.hdcio.com.cn/focus','http://www.sospc.cn/focus');
        $this->assign('appid',C('AppID'));
        $this->assign('config',$signPakg);
        if(I('get.istest')){
            $this->display('dynamic_test');
        }else $this->display('test');

    }
    public function main(){
        $wxjs   =   new  WeixinJs(C('AppID'),C('AppSecret'));
        $signPakg=$wxjs->getSignPackage(true);

        // $domain  =  array('http://www.sznfdx.com.cn/focus','http://www.hdcio.com.cn/focus','http://www.sospc.cn/focus');
        $this->assign('appid',C('AppID'));

        $this->assign('config',$signPakg);
        //$this->assign('url',$domain[rand(0,2)]);
        $this->display('index');
    }
    public function main2(){

        $wxjs   =   new  WeixinJs(C('AppID'),C('AppSecret'));
        $signPakg=$wxjs->getSignPackage(true);
        // $domain  =  array('http://www.sznfdx.com.cn/focus','http://www.hdcio.com.cn/focus','http://www.sospc.cn/focus');
        $this->assign('appid',C('AppID'));
        $this->assign('config',$signPakg);
        if(I('get.cls')){
            $static_class=M('static_class');
            $class_info = $static_class->where(['id'=>base64_decode(I('get.cls'))])->find();
            $top_img=explode(',',$class_info['top_imgs']);
            foreach($top_img as $key=>$item){
                $top_img[$key]='/weclass_static/img/'.$item;
            }
            $class_info['class_desc']="/weclass_static/img/".$class_info['class_desc'];
            $class_info['share_img']="/weclass_static/img/".$class_info['share_img'];
           // $class_info['class_detail_img']="/weclass_static/img/".$class_info['class_detail_img'];
            $class_info['description_img']="/weclass_static/img/".$class_info['description_img'];
            $class_info['audio']=C('CDN_URL').$class_info['audio'];
            //$class_detail_proc=explode(';;',$class_info['class_detail_procedure']);
            //$class_detail_error=explode(';;',$class_info['class_detail_error']);
            //$this->assign('class_detail_error',$class_detail_error);
            //$this->assign('class_detail_proc',$class_detail_proc);
            $this->assign('class_info',$class_info);
            $this->assign('top_img',$top_img[0]);
            $this->assign('jump_url',"http://www.liujialing35.cn/focus?fm=99");
            $this->display('test');
            exit();
        }
        if(I('get.id')){
            $dynamic_class=M('dynamic_class');
            $class_info = $dynamic_class->where(['id'=>base64_decode(I('get.id'))])->find();
            $top_img=explode(',',$class_info['top_img']);
            foreach($top_img as $key=>$item){
                $top_img[$key]='/weclass_static/img/'.$item;
            }
            $class_info['class_desc']="/weclass_static/img/".$class_info['class_desc'];
            $class_info['share_img']="/weclass_static/img/".$class_info['share_img'];
            $class_info['class_detail_img']="/weclass_static/img/".$class_info['class_detail_img'];
            $class_info['description_img']="/weclass_static/img/".$class_info['description_img'];
            $class_info['audio']="/weclass_static/audio/".$class_info['audio'];
            $class_detail_proc=explode(';;',$class_info['class_detail_procedure']);
            $class_detail_error=explode(';;',$class_info['class_detail_error']);
            $this->assign('class_detail_error',$class_detail_error);
            $this->assign('class_detail_proc',$class_detail_proc);
            $this->assign('class_info',$class_info);
            $this->assign('top_img',$top_img);
            $this->display('dmain2');
        }else $this->display('test2');
    }
    public function daily(){
        $wxjs   =   new  WeixinJs(C('AppID'),C('AppSecret'));
        $signPakg=$wxjs->getSignPackage(true);
        // $domain  =  array('http://www.sznfdx.com.cn/focus','http://www.hdcio.com.cn/focus','http://www.sospc.cn/focus');
        $this->assign('appid',C('AppID'));
        $this->assign('config',$signPakg);
        $this->display('testday2');
    }
    public function daily2(){
        $wxjs   =   new  WeixinJs(C('AppID'),C('AppSecret'));
        $signPakg=$wxjs->getSignPackage(true);
        // $domain  =  array('http://www.sznfdx.com.cn/focus','http://www.hdcio.com.cn/focus','http://www.sospc.cn/focus');
        $this->assign('appid',C('AppID'));
        $this->assign('config',$signPakg);
        $this->display('testday3');
    }

    public function newtest(){
        $wxjs   =   new  WeixinJs(C('AppID'),C('AppSecret'));
        $signPakg=$wxjs->getSignPackage(true);
        // $domain  =  array('http://www.sznfdx.com.cn/focus','http://www.hdcio.com.cn/focus','http://www.sospc.cn/focus');
        $this->assign('appid',C('AppID'));
        $this->assign('config',$signPakg);
        $static_class=M('static_class');
        $class_info = $static_class->where(['id'=>base64_decode(I('get.cls'))])->find();
        $top_img=explode(',',$class_info['top_imgs']);
        foreach($top_img as $key=>$item){
            $top_img[$key]='/weclass_static/img/'.$item;
        }
        $class_info['class_desc']="/weclass_static/img/".$class_info['class_desc'];
        $class_info['share_img']="/weclass_static/img/".$class_info['share_img'];
        // $class_info['class_detail_img']="/weclass_static/img/".$class_info['class_detail_img'];
        $class_info['description_img']="/weclass_static/img/".$class_info['description_img'];
        $class_info['audio']=C('CDN_URL').$class_info['audio'];
        //$class_detail_proc=explode(';;',$class_info['class_detail_procedure']);
        //$class_detail_error=explode(';;',$class_info['class_detail_error']);
        //$this->assign('class_detail_error',$class_detail_error);
        //$this->assign('class_detail_proc',$class_detail_proc);
        $this->assign('class_info',$class_info);
        $this->assign('top_img',$top_img[0]);
        $this->assign('jump_url',"http://www.liujialing35.cn/focus?fm=99");
        $this->display('test_new');
    }
}