<?php
namespace Weclass1\Controller;

use Think\Controller;
use Com\Weixin\WeixinJs;


/**
 * 模拟微信文章页面样式
 * @author codebean
 *
 */
class PassageController extends Controller
{
    /**
     * 阅读页面
     */
    public function read()
    {
        try {
            
            $id = I('id', 0, 'intval');
            if (empty($id)) {
                E('文章不存在', 404);
            }
            $passage = D("Passage");
            
            $where = array();
            $where['id'] = $id;
            $where['status'] = 1;
            
            $data = $passage->where($where)->find();
            if (empty($data)) {
                E('文章不存在', 404);
            }
            
            $data['create_time'] = date("Y-m-d", strtotime($data['create_time']));
            $data['content'] = ($data['content']);

            //替换图片路径为CDN全路径
            preg_match_all('/<img.+?src="(.+?)".+?\/>/i',  $data['content'] , $m );
            $cdn_url = C('CDN_URL');
            if( $m[1] )
            {
                foreach ($m[1] as $item)
                {
                    if(!preg_match('/^http.+/',$item)){
                        $data['content'] = str_replace($item,rtrim( $cdn_url , "/").$item, $data['content']);
                    }
                }
            }
            
            $wx  = new WeixinJs();
            $signPackage = $wx->getSignPackage();
            $this->assign('signPackage', $signPackage);
            
            $cd = I('cd', 0, 'intval');
            if ($cd && $this->is_microMessage && ! $this->is_subscribe) {
                session('passage_uid', $cd);
            }
            
            $this->assign('data', $data);
            
            $this->assign('is_microMessage', $this->is_microMessage);
            
            $this->assign('is_subscribe', $this->is_subscribe);
            
            $this->assign('userInfo', $this->userInfo);
            
            $this->display();
        } catch (\Exception $e) {
            $this->error( $e->getMessage() );
            die();
        }
    }

    
    //举报页面
    public function report()
    {
        try {
            $wx  = new WeixinJs();
            $signPackage = $wx->getSignPackage();
            $this->assign('signPackage', $signPackage);
            
            $this->display();
        } catch (\Exception $e) {
            $this->error( $e->getMessage() );
            die();
        }
    }
}