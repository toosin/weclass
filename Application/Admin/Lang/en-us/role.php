<?php
//中文语言包
return array(
		'role_add'=>'Role add',
		'role_list'=>'Role list',
		
		'role_name'=>'Role name',
		'role_enname'=>'Role English name',
		'role_description'=>'Role description',

		
		'role_name_tip'=>'Role name',
		'role_enname_tip'=>'Role English name',
		'role_description_tip'=>'Role description',
		
		
		
		'role_no_name'=>'Please fill out the role name',
		'role_edit_faild'=>'Modify the content is empty',
		

		
		'role_edit'=>'Edit',
		'role_delete'=>'Delete',
		'role_permissions'=>'Permissions',
		'no_role_tip'=>'Please check to give permission',
		
		'role_delete_tip'=>'Delete operation will clear user permissions in this role, please confirm whether you want to delete?',
		
		'role_cancel'=>'Cancel',
		
		'role_list_name'=>'Name',
		
		'role_user'=>'Member list',
		
		'role_permissions_edit'=>'Please operate below modify permissions',
		
);