<?php
//Index controller 英文语言包
return array(
	'dashboard'=>'Dashboard',
	'my_dashboard'=>'My Dashboard',
	'layout_options'=>'Layout Options',
	'fixed_header'=>'Fixed header',
	'fixed_ribbon'=>'Fixed Ribbon',
	'fixed_navigation'=>'Fixed Navigation',
	'clear_localstorage'=>'Clear Localstorage',
	'widget_reset'=>'Widget Reset',
	'choose_skins'=>'Choose Skins',
	'total_uv'=>'Total Users',
	'total_pv'=>'Total View',
	'total_click'=>'Total Clicks PV',
	'total_install'=>'Total Installs PV',
	'total_click_uv'=>'Total Click Users',
	'total_install_uv'=>'Total Install Users',
	'ad_uv'=>'Ad-Users',
	'ad_pv'=>'Ad-Views',
	'ad_click'=>'Ad-Clicks',
	'ad_install'=>'Ad-Installs',
	'ad_click_uv'=>'Ad-Click-Users',
	'ad_install_uv'=>'Ad-Install-Users',
	
	'table_global_data'=>'Global Data',
	'table_business_data'=>'Business Data',
	'table_content_data'=>'Content Data',
	'table_reference'=>'Reference',
	'table_click_rate'=>'Click UV/Total UV',
	'table_per_capita_show'=>'Per Capita show',
	'table_per_capita_click'=>'Per Capita click',
	'table_per_capita_install'=>'Per Capita install',
	
	'country_table'=>'Country',
	'country_users'=>'Users',
	'country_users_rate'=>'Users Rate',
	'country_clicks'=>'Clicks',
	'country_clicks_rate'=>'Clicks Rate',
	
	'version_title'=>'Version distribution(Product) ',
	'product_title'=>'Product Stats ',
	'country_title'=>'Country Stats ',
	'dashboard_edit'=>'Edit',
	
	'data_graph_title'=>'Data Graph',
	'data_graph_hour'=>'Hour Stats',
	'data_graph_day'=>'Day Stats',
	'data_graph_week'=>'Week Stats',

	'stats_by_hour'=>'Stats By Hour',
	'global_rate'=>'Global Rate',
	'business_rate'=>'Business Rate',
	'content_rate'=>'Content Rate',
	'global'=>'Global',
	'business'=>'Business',

	'product_line'=>'Product Line',
	'rate_by_day'=>'The rate by day',
	'rate_by_week'=>'The rate by week',
);