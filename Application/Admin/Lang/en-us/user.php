<?php
//中文语言包
return array(
		'user_add'=>'Add user',
		'user_list'=>'Manage users',
		'user_name'=>'Username',
		'user_email'=>'Email address',
		'user_phone'=>'Phone number',
		'user_pwd'=>'Password',
		'user_leader'=>'Leader name',
		'user_leader_email'=>'Leader email',
		'user_department'=>'Department',
		'user_lang'=>'Default language',
		'user_pic'=>'Pic',
		'user_last_ip'=>'Last login IP',
		'user_last_time'=>'Last login time',
		'user_status'=>'User state',
		'user_empty_prompt'=>'Field cannot be empty',
		'user_phone_error'=>'Phone number format is wrong',
		'user_email_error'=>'Email format is wrong',
		'user_pwd_error'=>'Password please control in 6 to 20 characters',
		'user_email_exist'=>'email is exist',
		'user_status_0'=>'LOCK',
		'user_status_1'=>'OPEN',
		'user_edit'=>'User edit',
		'user_permissions_edit'=>'Permissions',
		'user_pwd_edit_tip'=>'Don·t change not fill',
		'user_edit_faild'=>'edit the content is empty',
		
		'user_email_tip'=>'Please fill out the company email',
		'user_pwd_tip'=>'User initial password',
		'user_name_tip'=>'User\'s real name',
		'user_phone_tip'=>'User\'s phone number',
		'user_leader_tip'=>'User\'s leader name',
		'user_leaderemail_tip'=>'User\'s leader email',
		'user_department_tip'=>'User\'s department',
		'user_pwd_tip2'=>'User password',

		'user_role'=>'Role',
		'user_role_list'=>'Role manage',
		
		'user_add_pwd_tip'=>'System will send the initial password to the user mailbox, please don\'t worry about it',

);