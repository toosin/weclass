<?php
//中文语言包
return array(
		'role_add'=>'添加角色',
		'role_list'=>'角色列表',
		
		'role_name'=>'中文名称',
		'role_enname'=>'英文名称',
		'role_description'=>'描述',

		
		'role_name_tip'=>'角色中文名称',
		'role_enname_tip'=>'角色英文名称',
		'role_description_tip'=>'角色描述',
		
		
		
		'role_no_name'=>'请填写角色名称',
		'role_edit_faild'=>'修改内容为空',
		

		
		'role_edit'=>'编辑',
		'role_delete'=>'删除',
		'role_permissions'=>'权限',
		'no_role_tip'=>'请勾选要赋予的权限',
		
		'role_delete_tip'=>'删除操作将清空此角色下用户权限，请确认是否要删除？',
		
		'role_cancel'=>'取消',
		
		'role_list_name'=>'名称',
		
		'role_user'=>'成员列表',
		
		'role_permissions_edit'=>'请在下方操作修改权限',
		
);