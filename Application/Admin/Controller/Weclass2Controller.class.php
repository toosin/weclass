<?php
// +----------------------------------------------------------------------
// | Just do it
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------
// | Since: 2014-08-05 15:16
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Admin\Model\ToolModel;

/**
 * 程序框架基本页面，包括首页、菜单、锁屏等页面
 */
use Com\OSS\Client;
use Com\OSS\OssClient;
use Think\Controller;
use Common\Model\OpenIdModel;
class Weclass2Controller extends Controller
{
    protected $path_date='';
    public function addClass()
    {
    	//$this->assign("skin",I("cookie.skin","smart-style-3"));
    	//$this->assign("skin_logo", I("cookie.skin_logo","img/logo-pale.png") );
        //$this->display();
        $this->display('Weclass2/addclass');
        //dd
    }
    public function handle_add(){
        //var_dump($_POST);
       // var_dump($_FILES);

        //exit();
        $this->path_date  =  date('Ymd')."/";
        $img_path   =   WEB_ROOT.'/Public/weclass_static/img/'.$this->path_date;
        $audio_path =   WEB_ROOT.'/Public/weclass_static/audio/';
        $top_img=null;
        if(!is_dir($img_path)){
            mkdir($img_path);
        }

        $file_img_name=array();
        for($i=1;$i<=count($_FILES['back']['name']);$i++)
        {
                $allowedExts = array("gif", "jpeg", "jpg", "png");
                $temp = explode(".", $_FILES['back']["name"][$i-1]);
                $extension = end($temp);
                $file_final_name=time().$_FILES['back']["name"][$i-1];
                if ((($_FILES['back']["type"][$i-1] == "image/gif")
                        || ($_FILES['back']["type"][$i-1] == "image/jpeg")
                        || ($_FILES['back']["type"][$i-1] == "image/jpg")
                        || ($_FILES['back']["type"][$i-1] == "image/pjpeg")
                        || ($_FILES['back']["type"][$i-1] == "image/x-png")
                        || ($_FILES['back']["type"][$i-1] == "image/png"))
                    // 小于 3000 kb
                    && in_array($extension, $allowedExts))
                {
                    move_uploaded_file($_FILES['back']["tmp_name"][$i-1],  $img_path.$file_final_name );
                    $file_img_name[]= $this->path_date.$file_final_name;
                    $top_img=implode(',',$file_img_name);
                }else{
                    $this->error('上传错误:'. $_FILES["back"]["error"]);
                }
        }
        $class_desc =  $this->upload_img('class_desc',$img_path);
        $class_detail_img   =   $this->upload_img('class_detail_img',$img_path);
        $description_img = $this->upload_img('description_img',$img_path);
        //$desc_img=$this->upload_img('desc_img',$img_path);
        $share_img=$this->upload_img('share_img',$img_path);
        //$public_img=$this->upload_img('public_img',$img_path);
        $audio_name=time().$_FILES['audio']['name'];
        move_uploaded_file($_FILES['audio']['tmp_name'],$audio_path.$audio_name);
        try{
            $data   =   array(
               'article_title'=>I('post.title'),
               'top_img'=>$top_img,
               //'middle_desc'=>I('post.desc'),
                'class_detail_title'=>I('post.class_detail_title'),
                'class_detail_procedure'=>I('post.class_procedure'),
                'class_detail_img'=>$class_detail_img,
                'class_desc'=>$class_desc,
                'class_detail_error'=>I('post.usual_error'),
                'share_title'=>I('post.share_title'),
                'share_img'=>$share_img,
                'description_img'=>$description_img,
                'share_desc'=>I('post.share_desc'),
                'audio'=>$audio_name,
                'status'=>1,
                'addtime'=>date('Y-m-d H:i:s'),
                'group_num'=>mt_rand(1210,1538),
            );

            $class = M('dynamic_class');

            $id=$class->data($data)->add();
            //$class->getLastSql();
            //$this->success();
            $id=base64_encode($id);
            $url=U('class_list');
            echo "<script>alert('添加课程成功！课程链接为：http://".C('MAIN_DOMAIN')."/index/main2?id=$id');setTimeout(function(){location.href=\"http://\"+location.host+\"$url\"},3000)</script>";
        }catch(\Exception $e){
            $this->error($e->getMessage());
        }

    }
    private function upload_img($filename,$path){
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $_FILES[$filename]["name"]);
        $extension = end($temp);
        $file_final_name=time().$_FILES[$filename]["name"];
        if ((($_FILES[$filename]["type"] == "image/gif")
                || ($_FILES[$filename]["type"] == "image/jpeg")
                || ($_FILES[$filename]["type"] == "image/jpg")
                || ($_FILES[$filename]["type"] == "image/pjpeg")
                || ($_FILES[$filename]["type"] == "image/x-png")
                || ($_FILES[$filename]["type"] == "image/png"))
              // 小于 3000 kb
            && in_array($extension, $allowedExts))
        {
            move_uploaded_file($_FILES[$filename]["tmp_name"], $path .$file_final_name );
        }else{
            $this->error('上传错误');
        }

        return $this->path_date.$file_final_name;
    }
    public function class_list(){
          $class=M('static_class');
          $class_item   =  $class->where(['status'=>1])->select();
          foreach($class_item as $key=>$item){
              $class_item[$key]['link']='http://'.C('MAIN_DOMAIN').'index/main2?cls='.base64_encode($item['id']);
          }

          $this->assign('list',$class_item);
          $this->display('WeClass/classlist');
    }
    public function deleteArticle(){
        if(I('get.id')){
            try{
                $class=M('static_class');
                $class->where(['id'=>I('get.id')])->save(['status'=>0]);
                $this->success('删除成功！');

            }catch(\Exception $e){
                $this->error('删除失败！'.$e->getMessage());
            }

        }
    }

    public function handle_static_class(){
        var_dump($_FILES);
       // exit();
        $this->path_date  =  date('Ymd')."/";
        $img_path   =   WEB_ROOT.'/Public/weclass_static/img/'.$this->path_date;
        $audio_path =   WEB_ROOT.'/Public/weclass_static/audio/';
        $top_img=null;
        if(!is_dir($img_path)){
            mkdir($img_path);
        }

        $file_img_name=array();
        for($i=1;$i<=count($_FILES['back']['name']);$i++)
        {
            $allowedExts = array("gif", "jpeg", "jpg", "png");
            $temp = explode(".", $_FILES['back']["name"][$i-1]);
            $extension = end($temp);
            $file_final_name=time().$_FILES['back']["name"][$i-1];
            if ((($_FILES['back']["type"][$i-1] == "image/gif")
                    || ($_FILES['back']["type"][$i-1] == "image/jpeg")
                    || ($_FILES['back']["type"][$i-1] == "image/jpg")
                    || ($_FILES['back']["type"][$i-1] == "image/pjpeg")
                    || ($_FILES['back']["type"][$i-1] == "image/x-png")
                    || ($_FILES['back']["type"][$i-1] == "image/png"))
                // 小于 3000 kb
                && in_array($extension, $allowedExts))
            {
                move_uploaded_file($_FILES['back']["tmp_name"][$i-1],  $img_path.$file_final_name );
                $file_img_name[]= $this->path_date.$file_final_name;
                $top_img=implode(',',$file_img_name);
            }else{
                $this->error('上传错误:hhh'. $_FILES["back"]["error"]);
            }
        }
        $class_desc =  $this->upload_img('class_desc',$img_path);
        //$class_detail_img   =   $this->upload_img('class_detail_img',$img_path);
        $description_img = $this->upload_img('description_img',$img_path);
        //$desc_img=$this->upload_img('desc_img',$img_path);
        $share_img=$this->upload_img('share_img',$img_path);
        //$public_img=$this->upload_img('public_img',$img_path);
        $str="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        $audio_name=time().base64_encode($str,substr(mt_rand(0,46),3)).end(explode(',',$_FILES['audio']['name']));
        move_uploaded_file($_FILES['audio']['tmp_name'],$audio_path.$audio_name);
        $oss = new Client('beijing','LTAI7M7FOzwWAPdk','aw9KA5nolkYBu0zewJWjIo3SjPVp6G');
        $result = $oss->uploadFile('dushu-yujia',$audio_name,$audio_path.$audio_name);
        try{
            $data   =   array(
                'title'=>I('post.title'),
                'top_imgs'=>$top_img,
                //'middle_desc'=>I('post.desc'),
                'tid'=>I('post.tid'),
                'class_desc'=>$class_desc,
                'share_title'=>I('post.share_title'),
                'share_img'=>$share_img,
                'description_img'=>$description_img,
                'share_desc'=>I('post.share_desc'),
                'audio'=>$audio_name,
                'status'=>1,
                'addtime'=>date('Y-m-d H:i:s'),
                'group_num'=>mt_rand(1210,1538),
            );

            $class = M('static_class');

            $id=$class->data($data)->add();
            //$class->getLastSql();
            //$this->success();
            $id=base64_encode($id);
            $url=U('class_list');
            echo "<script>alert('添加课程成功！课程链接为：http://".C('MAIN_DOMAIN')."/index/main2?cls=$id');setTimeout(function(){location.href=\"http://\"+location.host+\"$url\"},3000)</script>";
        }catch(\Exception $e){
            $this->error($e->getMessage());
        }
    }

    public function getTheme(){
        $ret=array();
        try{
            $theme=M('theme');
            $theme_info=$theme->where(['status'=>1])->select();
            $ret['code']=200;
            $ret['data']=$theme_info;
        }catch(\Exception $e){
            $ret['code']=$e->getCode();
            $ret['data']=$e->getMessage();
        }

        $this->ajaxReturn($ret);
    }
    public function addtheme(){
        if($theme=I('post.theme')){
            $tmodel =   M('theme');
            $theme_info=$tmodel->where(['theme'=>$theme,'status'=>1])->select();
            if(!$theme_info['theme'])
            {
                $tmodel->data(['theme'=>$theme,'status'=>1,'addtime'=>date('Y-m-d H:i:s')])->add();
                echo "添加成功！";
            }else{
                echo "主题已存在";
            }
        }

    }
    public function own_test(){
        $oss = new Client('beijing','LTAI7M7FOzwWAPdk','aw9KA5nolkYBu0zewJWjIo3SjPVp6G');
        $result = $oss->uploadFile('dushu-yujia','benhansi.mp3',"/data/www/weclass/Public/weclass_static/audio/benhansi.mp3");
        $result1 = $oss->uploadFile('dushu-yujia','1506408507cunzhang_story.mp3',"/data/www/weclass/Public/weclass_static/audio/1506408507cunzhang_story.mp3");

        var_dump($result);
        var_dump($result1);
    }
}
