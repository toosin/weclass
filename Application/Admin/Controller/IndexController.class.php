<?php
// +----------------------------------------------------------------------
// | Just do it
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------
// | Since: 2014-08-05 15:16
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Admin\Model\ToolModel;

/**
 * 程序框架基本页面，包括首页、菜单、锁屏等页面
 */
use Think\Controller;
use Common\Model\OpenIdModel;
class IndexController extends BaseController
{
	/**
	 * 首页框架
     * @privilege public
	 * @return [type] [description]
	 */
    public function index()
    {
    	$this->assign("skin",I("cookie.skin","smart-style-3"));
    	$this->assign("skin_logo", I("cookie.skin_logo","img/logo-pale.png") );
        $this->display();
    }
    
    /**
     * 欢迎
     * @privilege public
     * @return [type] [description]
     */
    public function welcome()
    {
        
        $this->display();
    }

   
    /**
     * 左侧菜单栏
     * @return [type] [description]
     */
    public function menu()
    {
        $this->display( );
    }

    /**
     * [lock 锁屏]
     * @return [type] [description]
     */
    public function lock()
    {
        if( IS_POST ){
            try{
                $data['status']  = 200;
                $data['content'] = '';
                $data['msg']     = '';

                if( !IS_AJAX ){
                    E("access method is error", 401);
                }
                $user_id = session('userid');
                $pwd = I('post.password');
                if( empty($pwd) ){
                    E("please input password", 403);
                }
                $user = D('user');
                $where['userid'] = $user_id;
                $user_data = $user->where($where)->find();
                if( !$user_data ){
                    E("nobody find", 403);
                }
                $w_pwd = md5(md5($pwd).$user_data['pwd_key']);
                if($w_pwd!=$user_data['password']){
                    E("password error", 403);
                }
                //去掉锁屏的session标志位
                session('_lock_screen',null);
            }catch(\Exception $e){
                $data['msg'] = $e->getMessage();
                $data['status'] = $e->getCode();
            }
            $this->ajaxReturn($data);
        }

        session('_lock_screen',1);
        $this->display();
    }


    /**
     * 没有权限默认跳转到该页面
     * @privilege public
     * @return [type] [description]
     */
    public function noprivilege()
    {
        $this->display();
    }
    
    
}
