<?php
// +----------------------------------------------------------------------
// | 自动回复 控制器
// +----------------------------------------------------------------------
// | Author: dengsixian <1741159138@qq.com>
// +----------------------------------------------------------------------
// | Since: 2016-07-05 16:13:00
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Think\Controller;
use Think\Model;


class ReplyController extends BaseController
{
    const TABLE_NAME = 'wx_basic_reply';
    const TABLE_DATA =  array(
                ['name' => 'ID', 'us_name' => 'id', 'type' => 'pri_key', 'is_edit' => 0 ],
                ['name' => '名称', 'us_name' => 'name', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '关键字', 'us_name' => 'keyword', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '返回内容', 'us_name' => 'content', 'type' => 'textarea', 'is_edit' =>1 ],
                ['name' => '更新时间', 'us_name' => 'last_time', 'type' => 'date', 'is_edit' => 0 ],
                ['name' => '操作', 'us_name' => 'add'],
            );
    /**
     * 列表
     * @return [type] [description]
     */
    public function index()
    {
        $controller = I( 'get._c' );
        $action     = I( 'get._a' );
        //数据接口，转发到相应的action
        if( $action )
        {
            $controller = $controller ? $controller : CONTROLLER_NAME;
            R($controller."/".$action);
            return true;
        }

        $data = self::TABLE_DATA;
        $show_data = array_values($data);

        $json_data = '[
                        { "mDataProp": "id" ,"bSortable": true},
                        { "mDataProp": "name" ,"bSortable": false},
                        { "mDataProp": "keyword" ,"bSortable": false},
                        { "mDataProp": "content" ,"bSortable": false},
                        { "mDataProp": "last_time" ,"bSortable": false},
                        { "mDataProp": "edit" ,"bSortable": false},
                    ]';
        $this->assign(compact('show_data', 'json_data'));
        $this->display('Autohtml/index');
    }

    /**
     * [userdata description]
     * @return [type] [description]
     */
    public function dataList(){
        $ret = array();
        $ret['status'] = 200;
        $ret['msg'] = "ok";
        $ret['content'] = array();

        try{
            $offset = I("iDisplayStart",0,"intval");
            $size = I("iDisplayLength",10,"intval");

            $where = array();
            $keyword = I('sSearch');
            if( $keyword ) {
                $where['name']  = array('like','%'.$keyword.'%');
                $where['keyword']  = array('like','%'.$keyword.'%');
                $where['content']  = array('like','%'.$keyword.'%');
                $where['_logic'] = 'or';
            }
            $error_obj = D(self::TABLE_NAME);
            $data = $error_obj->where( $where )->limit("{$offset},{$size}")->order("id desc, last_time desc")->select();
            header("sql : ".$error_obj->getLastSql() );
            
            $ids = array();
            foreach ($data as $key=>$val){
            	$ids[ $val['id'] ] = $val['id'];

            }
            $is_focus_data = array();
            $ids = array_values($ids);
            foreach ($data as $key => $value) {
                $data[$key]['content'] = htmlspecialchars_decode($value['content'], true);
                $edit_url = '#'.C('SUB_PATH').'index.php?m=admin&c='.CONTROLLER_NAME.'&a=editData&id='.$value['id'];
                $data[$key]['edit'] = '<a href="'.$edit_url.'" title=""><i class="fa fa-edit fa-lg fa-fw txt-color-blue"></i>编辑</a> 
                        | <a onclick="dialog_del('.$value['id'].')" title="删除" href="javascript:void(0);"><i class="glyphicon glyphicon-trash txt-color-red"></i> 删除</a>';
            }
            $total = $error_obj->where( $where )->count();
            $rs1 = array();
            $rs1['data'] = $data?$data:array();
            $ret['iTotalDisplayRecords'] = $total?$total:0;
            $ret['iTotalRecords'] = $total?$total:0;

            $ret['content'] = $rs1;

        }catch(\Exception $e){
            $ret['status'] = $e->getCode();
            $ret['msg'] = $e->getMessage();
        }
        $this->ajaxReturn($ret);
    }

    /**
     * 添加
     */
    public function addData()
    {
        $post = I('post.');
        if($post['dosubmit']){
            unset($post['dosubmit']);
            $id =  M(self::TABLE_NAME)->add($post);
            if($id){
                $this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
            }
        }else{
            $this->assign('rel',array());
            $data = self::TABLE_DATA;
            $this->assign('data',$data);
            $this->display('Autohtml/edit');
        }
    }

    /**
     * 修改
     * @nav L('role_edit')
     */
    public function editData()
    {
        if(I('get.id')){
            $condition = [];
            $condition['id'] = intval(I('get.id'));
            $info = M(self::TABLE_NAME)->where($condition)->find();
            if(I('post.dosubmit')){
                $post = I('post.');
                unset($post['dosubmit']);
                // var_dump($post);exit;
                $rel = M(self::TABLE_NAME)->where($condition)->save($post);
                if($rel){
                    $this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
                }else{
                    $this->ajaxReturn( ajax_return_join(302, '', '更新失败'));exit;
                }
            }else{
                $this->assign('rel',$info);
                $data = self::TABLE_DATA;
                $this->assign('data',$data);
                $this->display('Autohtml/edit');
            }
        }else{
            redirect(U(CONTROLLER_NAME.'/index'));
        }
    }
    /**
     * 删除
     */
    public function delData()
    {
        if(I('post.id')){
            $data_obj = D(self::TABLE_NAME);
            $where = array('id'=>I('post.id'));
            $data_obj->where($where)->delete();
            $this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
        }else{
            redirect(U(CONTROLLER_NAME.'/index'));
        }
    }
}
