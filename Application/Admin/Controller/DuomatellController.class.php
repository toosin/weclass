<?php
// +----------------------------------------------------------------------
// | Just do it
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------
// | Since: 2014-08-05 15:16
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Admin\Model\ToolModel;

/**
 * 程序框架基本页面，包括首页、菜单、锁屏等页面
 */
use Com\OSS\Client;
use Com\OSS\OssClient;
use Common\Model\CategoryModel;
use Common\Model\ClassModel;
use Common\Model\SpecialModel;
use Think\Controller;
use Common\Model\OpenIdModel;
use Think\Upload;

class DuomatellController extends Controller
{
   public function addclass()
   {
      $this->display();
   }
   public function addhandler()
   {

      $result  =  array(
         'code'=>200,
          'msg'=>'upload success !'
      );
      $file_path  =  WEB_ROOT.'/Public/'.date('YMD');
      if(!is_dir($file_path)){
         mkdir($file_path);
      }

      $res=$this->uploadfile('poster_s',$file_path,'388000',['jpg','jpeg','png']);
      if($res['code']!=200){
         $result['code']=300;
         $result['msg']=$res['msg'];
         $this->ajaxReturn($result);
         die;
      }else{
         $image_s = $res['filename'];
      }

      $res=$this->uploadfile('poster_c',$file_path,'388000',['jpg','jpeg','png']);
      if($res['code']!=200){
         $result['code']=300;
         $result['msg']=$res['msg'];
         $this->ajaxReturn($result);
         die;
      }else{
         $image_c = $res['filename'];
      }

      $res  =  $this->uploadfile('audio',$file_path,'',['mp3','m4a','acc','wav']);
      if($res['code']!=200){
         $result['code']=300;
         $result['msg']=$res['msg'];
         $this->ajaxReturn($result);
         die;
      }else{
         $audio = $res['filename'];
      }

      $class_title=I('post.title');
      $sid  =  I('post.tid');
      $class_desc =  I('post.class_desc');
      try{
         $classmodel =  new ClassModel();
         $data=array(
             'title'=>$class_title,
             'desc'=>$class_desc,
             'sid'=>$sid,
             'poster_s'=>$image_s,
             'poster_c'=>$image_c,
             'audio'=>$audio,
             'status'=>1,
             'addtime'=>date('Y-m-d H:i:s')
         );
         $classmodel->data($data)->add();
      }catch(\Exception $e)
      {
         $result['code']=$e->getCode();
         $result['msg']=$e->getMessage();
      }
      $this->ajaxReturn($result);
   }

   private function uploadfile($file_name,$file_upload_path,$file_size_limit='',$allowtype=""){
      $result  =  array('code'=>200,'msg'=>'');
      if($allowtype){
         $allowedExts=$allowtype;
         $temp = explode(".", $_FILES[$file_name]["name"]);
         $extension = end($temp);     // 获取文件后缀名
         if (!in_array($extension, $allowedExts))
         {
            $result['code']=300;
            $result['msg']="file type is not allowed !";
            return $result;
         }
      }

      if($file_size_limit){
         if($_FILES[$file_name]["size"]>$file_size_limit)
         {
            $result['code']=300;
            $result['msg']="file size is up to limit:".($file_size_limit/1024)." KB !";
            return $result;
         }
      }

      if ($_FILES["file"]["error"] > 0)
      {
         //echo "错误：: " . $_FILES["file"]["error"] . "<br>";
         $result['code']=300;
         $result['msg']=$_FILES["file"]["error"];
         return $result;
      }

      $filename=sha1(microtime()).'.'.end(explode(".",$_FILES[$file_name]['name']));
      $upload=move_uploaded_file($_FILES[$file_name]["tmp_name"],$file_upload_path.'/'.$filename);
      if($upload){
         $result['filename']=$filename;
         $result['filepath']=$file_upload_path.'/'.$filename;
         $oss = new Client('beijing','LTAI7M7FOzwWAPdk','aw9KA5nolkYBu0zewJWjIo3SjPVp6G');
         $oss_up = $oss->uploadFile('dushu-yujia',$filename,$file_upload_path.'/'.$filename);
         if(!$oss_up){
            @unlink($file_upload_path.'/'.$filename);
         }else{
            $result['code']=300;
            $result['msg']=$oss_up;
         }
         return $result;
      }else{
         $result['filename']=$filename;
         $result['filepath']=$_FILES[$file_name]['tmp_name'];
         $oss = new Client('beijing','LTAI7M7FOzwWAPdk','aw9KA5nolkYBu0zewJWjIo3SjPVp6G');
         $oss_up = $oss->uploadFile('dushu-yujia',$filename,$file_upload_path.'/'.$filename);
         if(!$oss_up){
            @unlink($file_upload_path.'/'.$filename);
         }else{
            $result['code']=300;
            $result['msg']=$oss_up;
         }
         //return $result;
         return $result;
      }
   }

   public function add_special()
   {
      $this->display('addspecial');
   }

   public function special_handler()
   {
      $result  =  array(
          'code'=>200,
          'msg'=>'upload success !'
      );
      $file_path  =  WEB_ROOT.'/Public/'.date('YMD');
      if(!is_dir($file_path)){
         mkdir($file_path);
      }
      $res=$this->uploadfile('poster_s',$file_path,'388000',['jpg','jpeg','png']);
      if($res['code']!=200){
         $result['code']=300;
         $result['msg']=$res['msg'];
         $this->ajaxReturn($result);
         die;
      }else{
         $image_s = $res['filename'];
      }

      $res=$this->uploadfile('poster_c',$file_path,'388000',['jpg','jpeg','png']);
      if($res['code']!=200){
         $result['code']=300;
         $result['msg']=$res['msg'];
         $this->ajaxReturn($result);
         die;
      }else{
         $image_c = $res['filename'];
      }
      //var_dump($_POST);
      //var_dump($_FILES);
      try{
         $special_model =  new SpecialModel();
         $data =  array(
             'title'=>I('post.title'),
             'desc'=>I('post.special_desc'),
             'author'=>I('post.author'),
             'cid'=>I('post.cid'),
             'poster_c'=>$image_c,
             'poster_s'=>$image_s,
             'total'=>I('post.total'),
             'status'=>1,
             'addtime'=>date('Y-m-d H:i:s')
         );
         $special_model->data($data)->add();
      }catch(\Exception $e)
      {
         $result['code']=$e->getCode();
         $result['msg']=$e->getMessage();
      }
      $this->ajaxReturn($result);
   }

   public function add_category(){
      $this->display('addCategory');
   }
   public function category_handler()
   {
      $result  =  array(
          'code'=>200,
          'msg'=>'submit success !'
      );

      $file_path  =  WEB_ROOT.'/Public/'.date('YMD');
      if(!is_dir($file_path)){
         mkdir($file_path);
      }

      $res=$this->uploadfile('icon',$file_path,'388000',['jpg','jpeg','png']);

      if($res['code']!=200){
         $result['code']=300;
         $result['msg']=$res['msg'];
         $this->ajaxReturn($result);
         die;
      }else{
         $icon = $res['filename'];
      }
      //my_log()
      if($_FILES['top_icon']['name']&&I('post.top_title')){
         $res=$this->uploadfile('top_icon',$file_path,'388000',['jpg','jpeg','png']);

         if($res['code']!=200){
            $result['code']=300;
            $result['msg']=$res['msg'];
            $this->ajaxReturn($result);
            die;
         }else{
            $top_icon = $res['filename'];
         }
         $is_show_top =1;
         $top_title  =  I('post.top_title');
      }
      try{
         $model=new CategoryModel();
         $data =  array(
             'title'=>I('post.title'),
             'desc'=>I('post.desc'),
             'is_show_top'=>$is_show_top,
             'top_title'=>$top_title,
             'top_icon'=>$top_icon,
             'icon'=>$icon,
             'is_show'=>I('post.options')?1:0,
             'status'=>1,
             'addtime'=>date('Y-m-d H:i:s')
         );
         $model->data($data)->add();
      }catch(\Exception $e)
      {
         $result['code']=$e->getCode();
         $result['msg']=$e->getMessage();
      }

      $this->ajaxReturn($result);
   }

   public function getCategory(){
      $res =   array(
        'code'=>200,
        'data'=>''
      );
      try{
         $model=new CategoryModel();
         $categorys=$model->field('id,title')->where('status=1')->select();
         $res['data']=$categorys;
      }catch(\Exception $e)
      {
         my_log($e->getMessage());
      }
      $this->ajaxReturn($res);
   }

   public function getSpecial(){
      $res =   array(
          'code'=>200,
          'data'=>''
      );
      try{
         $model=new SpecialModel();
         $categorys=$model->field('id,title')->where('status=1')->select();
         $res['data']=$categorys;
      }catch(\Exception $e)
      {
         my_log($e->getMessage());
      }
      $this->ajaxReturn($res);
   }


}
