<?php
// +----------------------------------------------------------------------
// | Just do it
// +----------------------------------------------------------------------
// | Author: shioujun <shioujun@gmail.com>
// +----------------------------------------------------------------------
// | Since: 2014-08-05 15:16
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Admin\Model\ToolModel;

/**
 * 程序框架基本页面，包括首页、菜单、锁屏等页面
 */
use Think\Controller;
use Common\Model\OpenIdModel;
class WeclassController extends Controller
{

    public function addClass()
    {
    	//$this->assign("skin",I("cookie.skin","smart-style-3"));
    	//$this->assign("skin_logo", I("cookie.skin_logo","img/logo-pale.png") );
        //$this->display();
        $this->display('WeClass/addclass');
    }
    public function handle_add(){
        //var_dump($_POST);
        //var_dump($_FILES);
        $img_path   =   WEB_ROOT.'/Public/weclass_static/img/';
        $audio_path =   WEB_ROOT.'/Public/weclass_static/audio/';
        $top_back=$this->upload_img('back',$img_path);
        $desc_img=$this->upload_img('desc_img',$img_path);
        $share_img=$this->upload_img('share_img',$img_path);
        $public_img=$this->upload_img('public_img',$img_path);
        $audio_name=time().$_FILES['audio']['name'];
        move_uploaded_file($_FILES['audio']['tmp_name'],$audio_path.$audio_name);
    try{
        $data   =   array(
               'title'=>I('post.title'),
               'top_back'=>$top_back,
               'middle_desc'=>I('post.desc'),
               'middle_desc_img'=>$desc_img,
               'usage_img'=>$public_img,
                'share_title'=>I('post.share_title'),
                'share_img'=>$share_img,
                'share_desc'=>I('post.share_desc'),
                'audio'=>$audio_name,
                'status'=>1,
                'addtime'=>date('Y-m-d H:i:s'),
                'group_num'=>mt_rand(1210,1538),
            );
        //var_dump($data);
        //
            $class = M('class');
       // var_dump($class);

            $id=$class->add($data);
            //$class->getLastSql();
            //$this->success();
            $id=base64_encode($id);
            $url=U('class_list');
            echo "<script>alert('添加课程成功！课程链接为：http://".C('MAIN_DOMAIN')."/index/article?id=$id');setTimeout(function(){location.href=\"http://\"+location.host+\"$url\"},3000)</script>";
        }catch(\Exception $e){
            $this->error($e->getMessage());
        }

    }
    private function upload_img($filename,$path){
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $temp = explode(".", $_FILES[$filename]["name"]);
        $extension = end($temp);
        $file_final_name=time().$_FILES[$filename]["name"];
        if ((($_FILES[$filename]["type"] == "image/gif")
                || ($_FILES[$filename]["type"] == "image/jpeg")
                || ($_FILES[$filename]["type"] == "image/jpg")
                || ($_FILES[$filename]["type"] == "image/pjpeg")
                || ($_FILES[$filename]["type"] == "image/x-png")
                || ($_FILES[$filename]["type"] == "image/png"))
            && ($_FILES[$filename]["size"] < 3048000)    // 小于 3000 kb
            && in_array($extension, $allowedExts))
        {
            move_uploaded_file($_FILES[$filename]["tmp_name"], $path .$file_final_name );
        }else{
            $this->error('上传错误');
        }

        return $file_final_name;
    }
    public function class_list(){
          $class=M('class');
          $class_item   =  $class->where(['status'=>1])->select();
          foreach($class_item as $key=>$item){
              $class_item[$key]['link']='http://'.C('MAIN_DOMAIN').'/index/article?id='.base64_encode($item['id']);
          }
          $this->assign('list',$class_item);
          $this->display('WeClass/classlist');
    }
    public function deleteArticle(){
        if(I('get.id')){
            try{
                $class=M('class');
                $class->where(['id'=>I('get.id')])->save(['status'=>0]);
                $this->success('删除成功！');

            }catch(\Exception $e){
                $this->error('删除失败！'.$e->getMessage());
            }

        }
    }
}
