<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Exception;
class LoginController extends Controller {
	private $model;
	function __construct() {
		parent::__construct();
		$this->model = D('Admin');
	}
    /**
     * 登录
     *
    */
 	public function doLogin() {
    	if(I('post.dosubmit')){
    		$email = I('post.email');
    		$pwd = I('post.password');
    		if(!$pwd || !$email){
    			$this->ajaxReturn( ajax_return_join(201, '', 'login_empty_prompt'));exit;
    		}
    		if(strlen($pwd)>20 || strlen($pwd)<6){
    			$this->ajaxReturn( ajax_return_join(202, '', 'login_pwd_error'));exit;
    		}
//     		if( !I('post.email','',FILTER_VALIDATE_EMAIL) ){
//     			$this->ajaxReturn( ajax_return_join(203, '', 'login_email_error'));exit;
//     		}
    		$userinfo = $this->model->FindUser("email='".$email."'");
    		if(!$userinfo){
    			$this->ajaxReturn( ajax_return_join(404, '', 'login_nouser_info'));exit;
    		}
    		if($userinfo['status']!=1){
    			$this->ajaxReturn( ajax_return_join(302, '', 'login_status_info'));exit;
    		}
    		$w_pwd = md5(md5($pwd).$userinfo['pwd_key']);
    		if($w_pwd!=$userinfo['password']){
    			$this->ajaxReturn( ajax_return_join(202, '', 'login_pwd_error'));exit;
    		}
    		session('userid',$userinfo['userid']);
    		session('username',$userinfo['real_name']);
    		session('department',$userinfo['department']);
            session('user_avatar',$userinfo['pic']);
            session('user_email',$userinfo['email']);








            session('_lock_screen',null);
            if( empty($userinfo['lastloginip']) )
            {
                session('user_first_login',1);
            }
    		$arr = array(
    			'lastloginip'=>get_client_ip(),
                'lastlogintime'=>date('Y-m-d H:i:s')
    		);
    		$where = 'userid='.$userinfo['userid'];
    		$rel = $this->model->UpdataInfo($where,$arr);
    		$key = pwd_key();
    		session('md5', md5($email.$key).$key);
    		#获取权限
    		$adminer = C('ADMINISTRATOR_USERID');
    		if(!in_array($userinfo['userid'],$adminer)){
    			$role_user_module = D('Roleuser');
    			$where = 'user_id='.$userinfo['userid'];
    			$Rolepermissions = $role_user_module->SelectRole($where,'role_id');
    			foreach($Rolepermissions as $key=>$val){
    				$userrole[] = $val['role_id'];
                }
    			if(!$userrole){
    				session('[destroy]');
    				$this->ajaxReturn( ajax_return_join(403, '', 'no_role_priv'));exit;
    			}
    			$Rolepermissions_model = D('Rolepermissions');
    			$privwhere = 'role_id in('.implode(',', $userrole).')';
    			$Rolepermissions = $Rolepermissions_model->SelectRole($privwhere,'per_menu_id');

    			foreach($Rolepermissions as $key=>$val){
    				$priv[] = $val['per_menu_id'];
    			}

    			if(!$priv){
    				session('[destroy]');
    				$this->ajaxReturn( ajax_return_join(403, '', 'no_role_priv'));exit;
    			}
    			session('priv_id',$priv);
    			#获取所有控制器方法
    			$menu_model = D('Menu');
    			$privwhere = 'id in('.implode(',', $priv).')';
    			$menus = $menu_model->getMenu($privwhere);
    			foreach($menus as $key=>$val){
    				$priv_menu[] = $val['module'].'_'.$val['controller'].'_'.$val['action'];
    			}
    			//特殊校验首页授权
    			/*if(!in_array('admin_index_dashboard',$priv_menu)){
    				$this->ajaxReturn( ajax_return_join(403, array('no_index'), 'no_role_priv_index'));exit;
    			}*/
    			session('priv_menu',$priv_menu);
    		}
    		$this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;

    	}else{
    		$this->display();
    	}
    }

    /**
     * 注销退出
     *
    */
    public function logout() {
    	session('[destroy]');
    	redirect(U('login/doLogin'));
    }
    public function _empty() {
    	echo "empty action";
    }
}
