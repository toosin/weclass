<?php
// +----------------------------------------------------------------------
// | 后台菜单 控制器
// +----------------------------------------------------------------------
// | Author: dengsixian <1741159138@qq.com>
// +----------------------------------------------------------------------
// | Since: 2016-07-05 16:13:00
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Think\Controller;
use Think\Model;


class MenuController extends BaseController
{

    // 公用 创建后台方法配置：
    // 注意配置： 模糊 搜索字段
    //             order 排序字段

    //表名
    const TABLE_NAME = 'menu';
    const TABLE_DATA =  array(
                //name 名称
                //us_name 英文名 一般为表字段
                //type form表单类型  (text|redio|textarea)
                //is_edit 是否要编辑
                ['name' => 'ID', 'us_name' => 'id', 'type' => 'pri_key', 'is_edit' => 0 ],
                ['name' => '名称', 'us_name' => 'name', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '英文名称', 'us_name' => 'en_name', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => 'module', 'us_name' => 'module', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => 'controller', 'us_name' => 'controller', 'type' => 'text', 'is_edit' =>1 ],
                ['name' => 'action', 'us_name' => 'action', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '参数', 'us_name' => 'param', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => 'pid', 'us_name' => 'pid', 'type' => 'select', 'is_edit' => 1 ],
                ['name' => 'uid', 'us_name' => 'uid', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '权重', 'us_name' => 'orderid', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '是否显示', 'us_name' => 'show', 'type' => 'redio', 'is_edit' => 1 ],
                ['name' => '图标', 'us_name' => 'icon', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '自定义URL', 'us_name' => 'url', 'type' => 'text', 'is_edit' => 1 ],
                ['name' => '操作', 'us_name' => 'edit'],
            );
    /**
     * 列表
     * @return [type] [description]
     */
    public function index()
    {
        $controller = I( 'get._c' );
        $action     = I( 'get._a' );
        //数据接口，转发到相应的action
        if( $action )
        {
            $controller = $controller ? $controller : CONTROLLER_NAME;
            R($controller."/".$action);
            return true;
        }

        $data = self::TABLE_DATA;
        $show_data = array_values($data);

        $arr = array();
        foreach ($data as $key => $val) {
            // if($val['us_name'] == 'add') continue;
            $arr[] = array(
                'mDataProp' => $val['us_name'], 'bSortable' => $val['us_name'] == 'id' ? true : false);
        }
        $json_data = json_encode($arr);

        $this->assign(compact('show_data', 'json_data'));
        $this->display('Autohtml/index');
    }

    /**
     * [userdata description]
     * @return [type] [description]
     */
    public function dataList(){
        $ret = array();
        $ret['status'] = 200;
        $ret['msg'] = "ok";
        $ret['content'] = array();

        try{
            $offset = I("iDisplayStart",0,"intval");
            $size = I("iDisplayLength",10,"intval");

            $where = array();
            $keyword = I('sSearch');
            if( $keyword ) {
                $where['name']  = array('like','%'.$keyword.'%');
                $where['keyword']  = array('like','%'.$keyword.'%');
                $where['content']  = array('like','%'.$keyword.'%');
                $where['_logic'] = 'or';
            }
            $error_obj = D(self::TABLE_NAME);
            $data = $error_obj->where( $where )->limit("{$offset},{$size}")->order("id desc")->select();
            header("sql : ".$error_obj->getLastSql() );
            
            $ids = array();
            foreach ($data as $key=>$val){
                $ids[ $val['id'] ] = $val['id'];

            }
            $is_focus_data = array();
            $ids = array_values($ids);
            foreach ($data as $key => $value) {
                $data[$key]['content'] = htmlspecialchars_decode($value['content'], true);
                $edit_url = '#'.C('SUB_PATH').'index.php?m=admin&c='.CONTROLLER_NAME.'&a=editData&id='.$value['id'];
                $data[$key]['edit'] = '<a href="'.$edit_url.'" title=""><i class="fa fa-edit fa-lg fa-fw txt-color-blue"></i>编辑</a> 
                        | <a onclick="dialog_del('.$value['id'].')" title="删除" href="javascript:void(0);"><i class="glyphicon glyphicon-trash txt-color-red"></i> 删除</a>';
            }
            $total = $error_obj->where( $where )->count();
            $rs1 = array();
            $rs1['data'] = $data?$data:array();
            $ret['iTotalDisplayRecords'] = $total?$total:0;
            $ret['iTotalRecords'] = $total?$total:0;

            $ret['content'] = $rs1;

        }catch(\Exception $e){
            $ret['status'] = $e->getCode();
            $ret['msg'] = $e->getMessage();
        }
        $this->ajaxReturn($ret);
    }

    public function selectData($pid='')
    {
        $condition['pid'] = 0;
        $condition['show'] = 1;
        $data = M('menu')->where($condition)->select();
        $html = '';
        foreach($data as $value)
        {
            $selected = $value['id']==$pid ? 'selected="selected"' : '';
            $html .= '<option value="'.$value['id'].'" '.$selected.'> '.$value['name'].'</option>';
        }
        $select_data = $html;
//        var_dump($select_data)
        $this->assign(compact('select_data'));
    }

    /**
     * 添加
     */
    public function addData()
    {
        $post = I('post.');
        $this->selectData();
        if($post['dosubmit']){
            unset($post['dosubmit']);
            $id =  M(self::TABLE_NAME)->add($post);
            if($id){
                $this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
            }
        }else{
            $this->assign('rel',array());
            $data = self::TABLE_DATA;
            $this->assign('data',$data);
            $this->display('Autohtml/edit');
        }
    }

    /**
     * 修改
     * @nav L('role_edit')
     */
    public function editData()
    {
        if(I('get.id')){
            $condition = [];
            $condition['id'] = intval(I('get.id'));
            $info = M(self::TABLE_NAME)->where($condition)->find();
            if(I('post.dosubmit')){
                $post = I('post.');
                unset($post['dosubmit']);
                // var_dump($post);exit;
                $rel = M(self::TABLE_NAME)->where($condition)->save($post);
                if($rel){
                    $this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
                }else{
                    $this->ajaxReturn( ajax_return_join(302, '', '更新失败'));exit;
                }
            }else{
//                var_dump($info);exit;
                $this->selectData($info['pid']);
                $this->assign('rel',$info);
                $data = self::TABLE_DATA;
                $this->assign('data',$data);
                $this->display('Autohtml/edit');
            }
        }else{
            redirect(U(CONTROLLER_NAME.'/index'));
        }
    }
    /**
     * 删除
     */
    public function delData()
    {
        if(I('post.id')){
            $data_obj = D(self::TABLE_NAME);
            $where = array('id'=>I('post.id'));
            $data_obj->where($where)->delete();
            $this->ajaxReturn( ajax_return_join(200, '', 'cms_success'));exit;
        }else{
            redirect(U(CONTROLLER_NAME.'/index'));
        }
    }
}
