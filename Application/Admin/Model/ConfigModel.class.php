<?php

namespace Admin\Model;
use Think\Model;
class ConfigModel  extends Model{
	protected $tableName = 'config';
	
	public function gets( $key ){
		$where = array();
		$where['key'] = $key;
		return  $this->where( $where )->find();
	}
	
	public function sets($key,$val){
		if( is_array($val) ){
			$val = serialize($val);
		}
		$rs = $this->gets($key);
		if( $rs ){
			$where = array();
			$where['id'] = $rs['id'];

			$data = array();
			$data['value'] = $val;
			$data['update_time'] = date("Y-m-d H:i:s");
			return $this->where($where)->save($data);
		}else{
			$data = array();
			$data['key'] = $key;
			$data['value'] = $val;
			$data['update_time'] = date("Y-m-d H:i:s");
			return $this->add($data);
		}
	}
	
	public function getAll(){
		$config = D("Config");
		$datas = $config->select();
		$data = array();
		foreach ($datas as $k=>$v){
			$data[$v['key']] = ( $v['value'] );
		}
		return $data;
	}
}