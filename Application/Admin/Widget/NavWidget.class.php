<?php
namespace Admin\Widget;
use Think\Controller;

class NavWidget extends Controller {
	
	/**
	 * [navTop 页面头部导航菜单]
	 * @return [type] [description]
	 */
	public function navTop() {
		$return = array();
		$method = new \ReflectionMethod(MODULE_NAME."\Controller\\".CONTROLLER_NAME."Controller", ACTION_NAME);
		$method_doc = $method->getDocComment();
		if( !empty($method_doc) ){
			preg_match("/\@nav(.+)/", $method_doc,$mat);
			if( $mat[1] ) {
				$return = explode(",",trim( $mat[1] ));
			}
		}
		$return_str = '';
		$count = count( $return );
		foreach ($return as $key => $value) {
			$value = trim( $value );
			//解析语言包
			if( preg_match('/L\(.+?\)/', $value) ) {
			 	eval('$value = '.$value.";" );
			}
			if( 0==$key ){
				$return_str .= $value;
			}else{
				$return_str .= '<span>'.$value.'</span>';
			}
			if( $key!== ($count-1) ) {
				$return_str .=" > ";
			}
		}

		return $return_str;
	}
}