<?php
namespace Common\Model;

use Think\Model;

class OpenIdModel extends Model
{

    protected $tableName = 'openid';

    /**
     * openid转uid
     * @param unknown $openid
     * @param string $wxhao
     */
    public function openid2Uid($openid , $wxhao="" )
    {
        $where = array();
        $where['openid'] = $openid;
        if( $wxhao )
        {
            $where['wxhao'] = $wxhao;
        }else if( $wxhao==="" ){
            $where['wxhao'] = C('WX_HAO');
        }
        $open_info = $this->field('uid')
            ->where($where)
            ->find();
        
        return $open_info['uid'] ? $open_info['uid'] : false;
    }
    
    /**
     * uid转openid
     * @param unknown $uid
     * @param string $wxhao
     * @return boolean
     */
    public function uid2Openid( $uid , $wxhao="" )
    {
        $where = array();
        $where['uid'] = $uid;
        
        if( $wxhao )
        {
            $where['wxhao'] = $wxhao;
        }else if( $wxhao==="" ){
            $where['wxhao'] = C('WX_HAO');
        }
        
        $open_info = $this->field('openid')
        ->where($where)
        ->find();
    
        return $open_info['openid'] ? $open_info['openid'] : false;
    }
    
    /**
     * uid转openid info
     * @param unknown $uid
     * @param string $wxhao
     * @return boolean
     */
    public function uid2OpenidInfo( $uid , $wxhao="" )
    {
        $where = array();
        $where['uid'] = $uid;
    
        if( $wxhao )
        {
            $where['wxhao'] = $wxhao;
        }else if( $wxhao==="" ){
            $where['wxhao'] = C('WX_HAO');
        }
    
        $open_info = $this->where($where)->find();
    
        return $open_info ? $open_info: false;
    }
}