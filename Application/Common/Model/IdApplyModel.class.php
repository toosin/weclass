<?php
namespace Common\Model;

use Think\Model;

/**
 * 获取数据表自动增长ID
 *
 * @author codebean
 *
 */
class IdApplyModel extends Model
{

    protected $tableName = 'id_apply';

    /**
     * 申请 自增长ID
     * 
     * @param unknown_type $type            
     * @return int id
     */
    public function apply($type = 'user')
    {
        $data = array();
        $data['type'] = $type;
        return $this->add($data);
    }
}