<?php
// +----------------------------------------------------------------------
// | 一个可在后台设置自动消息回复与 编辑微信菜单的功用
// +----------------------------------------------------------------------
// | Since: 2016-05-26 14:54
// +----------------------------------------------------------------------
// | Author: dengsixian <1741159138@qq.com>
// +----------------------------------------------------------------------

namespace Common\Model;

use Think\Model;

/**
 * 数据库配置
 *
 * @author codebean
 *
 */
class WxBasicReplyModel extends Model
{

    protected $tableName = 'wx_basic_reply';

    public function answer($keyword) {

        $basic_arr = $this->select();
        $content = "";
        $default_answer = "未设置默认回复";
        foreach ((array)$basic_arr as $key => $value) {
            // 默认回复
            if($value['keyword'] == 'default_answer') {
                $default_answer = $value['content'];
                // break;
            }
            // 模糊查找 
            if(stristr($value['keyword'], '%')) {
                $k_word = str_replace('%', '', $value['keyword']);
                if(stristr($keyword, $k_word)) {
                    $content = $value['content'];
                    // break;
                }
            }
            // 匹配模式
            if($value['keyword'] == $keyword) {
                $content = $value['content'];
                // break;
            }
        }
        return $content ? $content : $default_answer;
    }
}