<?php
return array(
    // 数据库配置信息
    'DB_HOST' => 'rm-2ze11bm754mqfdm95.mysql.rds.aliyuncs.com',
    'DB_USER' => 'xiaoyu_pro',
    'DB_PWD' => 'f39676201ce477cac5ec794c619ab164',
    'DB_TYPE' => 'mysqli', // 数据库类型
    'DB_NAME' =>  'weclass2',
    'DB_PORT' => 3306, // 端口
    'DB_PREFIX' => 'bs_', // 数据库表前缀
    
    //默认MOUDLE
    'DEFAULT_MODULE' => 'Weclass1',
    //允许访问的MODULE
    'MODULE_ALLOW_LIST' => array(
        'Weclass1'
    ),
    
    //URL忽略大小写
    'URL_CASE_INSENSITIVE' => true,
    //REWRITE模式
    'URL_MODEL' => 1,
    
//     'MEMCACHE_HOST' => '127.0.0.1',
//     'MEMCACHE_PORT' => '11211',
    
//     'SESSION_TYPE' => 'Memcache',
    'SESSION_OPTIONS' => array(
        'name' => 'Weclass2',
        'expire' => 1800
    ),
    
    'SESSION_PREFIX' => 'Weclass2',
    'SESSION_DOMAIN' => "",
    
//     'DATA_CACHE_TYPE' => 'Memcache',
    'DATA_CACHE_PREFIX' => 'Weclass1',
    
    
    /*-------一下为自定义配置-------*/
    'AppID'=>'wx25b4510a53b4a917',
    'AppSecret'=>'a96e0c9d5afa0307c7e0d5f1cda8eba5',
    /*'AppID'=>'wxdb3d59fd0558c323',
    'AppSecret'=>'2bb19aeaec8f17f6cfde9c2135fa677d',*/
    /*'AppID' => 'wx2d0bd04d5384b8a6',
    'AppSecret' => 'fe04100ab06f76cc428d861c2c68e236',*/
    'TOKEN' => "test",
    'EncodingAESKey' => 'test',
    //当前微信号的名称
    'WX_NAME' => '测试',
    //当前微信号的别名
    'WX_ALIAS' => '测试',
    //当前微信号，如果无，请填写微信名称的全拼
    'WX_HAO' => 'soj_kangzi',

    /**
     * 域名相关
     */
    //主域名
    'MAIN_DOMAIN' => "www.vvxko.cn/",
    //CDN域名，如果没有CDN请填写主域名的
    'CDN_DOMAIN' => "cdn.liujialing45.cn",
    //CDN的URL
    'CDN_URL' => 'http://cdn.liujialing45.cn/',

    //当前服务器的唯一标示
    'CLUSTER_ID'=>'1',
    
    
    //微信支付相关配置（当使用微信支付的时候配置），具体参数的详细说明请查看微信商户平台的文档
    "QIYE_PAY_URL"=>"https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers",
    "HONGBAO_PAY_URL"=>"https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack",
    "MCH_ID"=>"1297808501",
    "API_KEY"=>"8c949f5f0671054f6920af7a565c3475",
    "CERT_PATH"=>"/Application/Fenxiao/Cert/demo/apiclient_cert.pem",
    "CERT_KEY_PATH"=>"/Application/Fenxiao/Cert/demo/apiclient_key.pem",
    'NOTIFY_URL'=>'http://soj.demo.com/service/weixinNotify',
    'REPORT_LEVENL'=>0,
    'CURL_PROXY_HOST'=>'0.0.0.0',
    'CURL_PROXY_HOST'=>0,
    'APP_NAME'=>'商品',
    'NICK_NAME'=>'商品',

    //搜索音乐的数量
    'MUSIC_NUMBER' => 5,
    
);